<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:2294621c-18a6-49a4-8842-bde47c948296(org.campagnelab.book.diagrams.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="qq7b" ref="r:2e3d7ff3-5fde-466c-9d84-5a929b65482a(org.campagnelab.book.diagrams.structure)" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz" />
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2" />
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="1151688443754" name="jetbrains.mps.baseLanguage.collections.structure.ListType" flags="in" index="_YKpA">
        <child id="1151688676805" name="elementType" index="_ZDj9" />
      </concept>
      <concept id="1151689724996" name="jetbrains.mps.baseLanguage.collections.structure.SequenceType" flags="in" index="A3Dl8">
        <child id="1151689745422" name="elementType" index="A3Ik2" />
      </concept>
      <concept id="1237721394592" name="jetbrains.mps.baseLanguage.collections.structure.AbstractContainerCreator" flags="nn" index="HWqM0">
        <child id="1237721435807" name="elementType" index="HW$YZ" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1160600644654" name="jetbrains.mps.baseLanguage.collections.structure.ListCreatorWithInit" flags="nn" index="Tc6Ow" />
      <concept id="1201792049884" name="jetbrains.mps.baseLanguage.collections.structure.TranslateOperation" flags="nn" index="3goQfb" />
    </language>
  </registry>
  <node concept="13h7C7" id="5$vcisXu1Gs">
    <ref role="13h7C2" to="qq7b:OL$4j00qWr" resolve="Project" />
    <node concept="13i0hz" id="5$vcisXu1M0" role="13h7CS">
      <property role="TrG5h" value="connections" />
      <node concept="3Tm1VV" id="5$vcisXu1M1" role="1B3o_S" />
      <node concept="A3Dl8" id="5$vcisXu1Ul" role="3clF45">
        <node concept="3Tqbb2" id="5$vcisXu1Uq" role="A3Ik2" />
      </node>
      <node concept="3clFbS" id="5$vcisXu1M3" role="3clF47">
        <node concept="3SKdUt" id="5$vcisXuyr6" role="3cqZAp">
          <node concept="3SKdUq" id="5$vcisXuyut" role="3SKWNk">
            <property role="3SKdUp" value="return a list of PackageRef instances" />
          </node>
        </node>
        <node concept="3cpWs8" id="5$vcisXuh9a" role="3cqZAp">
          <node concept="3cpWsn" id="5$vcisXuh9d" role="3cpWs9">
            <property role="TrG5h" value="result" />
            <node concept="_YKpA" id="5$vcisXuh98" role="1tU5fm">
              <node concept="3Tqbb2" id="5$vcisXuh9t" role="_ZDj9" />
            </node>
            <node concept="2ShNRf" id="5$vcisXuhui" role="33vP2m">
              <node concept="Tc6Ow" id="5$vcisXuhue" role="2ShVmc">
                <node concept="3Tqbb2" id="5$vcisXuhuf" role="HW$YZ" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5$vcisXuhyd" role="3cqZAp">
          <node concept="2OqwBi" id="5$vcisXuiHC" role="3clFbG">
            <node concept="2OqwBi" id="5$vcisXuh$M" role="2Oq$k0">
              <node concept="13iPFW" id="5$vcisXuhyb" role="2Oq$k0" />
              <node concept="3Tsc0h" id="5$vcisX_RVU" role="2OqNvi">
                <ref role="3TtcxE" to="qq7b:5$vcisXyyM4" />
              </node>
            </node>
            <node concept="3goQfb" id="5$vcisX_S$m" role="2OqNvi">
              <node concept="1bVj0M" id="5$vcisX_S$o" role="23t8la">
                <node concept="Rh6nW" id="5$vcisX_S$v" role="1bW2Oz">
                  <property role="TrG5h" value="p" />
                  <node concept="2jxLKc" id="5$vcisX_S$w" role="1tU5fm" />
                </node>
                <node concept="3clFbS" id="5$vcisX_TkA" role="1bW5cS">
                  <node concept="3clFbF" id="5$vcisX_TrT" role="3cqZAp">
                    <node concept="2OqwBi" id="5$vcisX_Tx4" role="3clFbG">
                      <node concept="37vLTw" id="5$vcisX_TrS" role="2Oq$k0">
                        <ref role="3cqZAo" node="5$vcisX_S$v" resolve="p" />
                      </node>
                      <node concept="3Tsc0h" id="5$vcisX_UBO" role="2OqNvi">
                        <ref role="3TtcxE" to="qq7b:5$vcisX$34z" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5$vcisXuhvj" role="3cqZAp">
          <node concept="37vLTw" id="5$vcisXuhvi" role="3clFbG">
            <ref role="3cqZAo" node="5$vcisXuh9d" resolve="result" />
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="5$vcisXu1Lb" role="13h7CW">
      <node concept="3clFbS" id="5$vcisXu1Lc" role="2VODD2" />
    </node>
  </node>
</model>

