<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:e59d77a3-4c48-46fc-894f-757799828fa9(org.campagnelab.book.diagrams.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="-1" />
    <use id="6106f611-7a74-42d1-80de-edc5c602bfd1" name="jetbrains.mps.lang.editor.diagram" version="-1" />
    <use id="93bc01ac-08ca-4f11-9c7d-614d04055dfb" name="org.campagnelab.mps.editor2pdf" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="qq7b" ref="r:2e3d7ff3-5fde-466c-9d84-5a929b65482a(org.campagnelab.book.diagrams.structure)" />
    <import index="8tro" ref="r:257a7f19-40a4-4037-a93b-ce1b638af281(jetbrains.mps.lang.editor.figures.library)" />
    <import index="goga" ref="r:2294621c-18a6-49a4-8842-bde47c948296(org.campagnelab.book.diagrams.behavior)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="93bc01ac-08ca-4f11-9c7d-614d04055dfb" name="org.campagnelab.mps.editor2pdf">
      <concept id="893392931327129896" name="org.campagnelab.mps.editor2pdf.structure.DiagramOutputDirectory" flags="ng" index="KZc4b">
        <property id="893392931327129956" name="path" index="KZc57" />
      </concept>
    </language>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi">
        <child id="1078153129734" name="inspectedCellModel" index="6VMZX" />
        <child id="2597348684684069742" name="contextHints" index="CpUAK" />
      </concept>
      <concept id="6822301196700715228" name="jetbrains.mps.lang.editor.structure.ConceptEditorHintDeclarationReference" flags="ig" index="2aJ2om">
        <reference id="5944657839026714445" name="hint" index="2$4xQ3" />
      </concept>
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1237308012275" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineStyleClassItem" flags="ln" index="ljvvj" />
      <concept id="1237375020029" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineChildrenStyleClassItem" flags="ln" index="pj6Ft" />
      <concept id="1237385578942" name="jetbrains.mps.lang.editor.structure.IndentLayoutOnNewLineStyleClassItem" flags="ln" index="pVoyu" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="5944657839000868711" name="jetbrains.mps.lang.editor.structure.ConceptEditorContextHints" flags="ig" index="2ABfQD">
        <child id="5944657839000877563" name="hints" index="2ABdcP" />
      </concept>
      <concept id="5944657839003601246" name="jetbrains.mps.lang.editor.structure.ConceptEditorHintDeclaration" flags="ig" index="2BsEeg">
        <property id="168363875802087287" name="showInUI" index="2gpH_U" />
        <property id="5944657839012629576" name="presentation" index="2BUmq6" />
      </concept>
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1233758997495" name="jetbrains.mps.lang.editor.structure.PunctuationLeftStyleClassItem" flags="ln" index="11L4FC" />
      <concept id="1236262245656" name="jetbrains.mps.lang.editor.structure.MatchingLabelStyleClassItem" flags="ln" index="3mYdg7">
        <property id="1238091709220" name="labelName" index="1413C4" />
      </concept>
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="6106f611-7a74-42d1-80de-edc5c602bfd1" name="jetbrains.mps.lang.editor.diagram">
      <concept id="1094405431463454433" name="jetbrains.mps.lang.editor.diagram.structure.CellModel_DiagramNode" flags="sg" stub="730538219795610279" index="9$NOg">
        <child id="1094405431463455193" name="figure" index="9$N8C" />
        <child id="1094405431463761842" name="parameters" index="9_WL3" />
        <child id="2084788800269090635" name="inputPort" index="zbHsl" />
        <child id="2084788800269090678" name="outputPort" index="zbHsC" />
      </concept>
      <concept id="1094405431463761863" name="jetbrains.mps.lang.editor.diagram.structure.FigureParameterMapping" flags="ng" index="9_WKQ">
        <child id="285670992218957021" name="argument" index="3YbGMt" />
      </concept>
      <concept id="6306886970791033847" name="jetbrains.mps.lang.editor.diagram.structure.CellModel_Diagram" flags="sg" stub="730538219795567478" index="2b3QIZ">
        <child id="6619018968336658044" name="paletteDeclaration" index="2qB1ji" />
        <child id="8570854907290721333" name="elementsCreation" index="3cyXsl" />
        <child id="939897302409114961" name="connectorCreation" index="3Iu_Fc" />
        <child id="5355858557208539148" name="diagramElements" index="1VXmjR" />
      </concept>
      <concept id="1301388602725986966" name="jetbrains.mps.lang.editor.diagram.structure.AbstractDiagramCreation" flags="ng" index="mdwis">
        <reference id="1301388602726005553" name="concept" index="mdGOV" />
        <child id="1301388602726005547" name="query" index="mdGOx" />
      </concept>
      <concept id="526297864816328068" name="jetbrains.mps.lang.editor.diagram.structure.Palette" flags="ng" index="2p8riq">
        <child id="526297864816428346" name="elements" index="2p8WK$" />
      </concept>
      <concept id="6619018968335599081" name="jetbrains.mps.lang.editor.diagram.structure.CreationActionReference" flags="ng" index="2qV3X7">
        <reference id="6619018968336102388" name="elementsCreation" index="2qTo_q" />
      </concept>
      <concept id="6382742553261733065" name="jetbrains.mps.lang.editor.diagram.structure.CellModel_DiagramConnector" flags="sg" stub="730538219795610242" index="2FuRD1">
        <child id="1220375669566529919" name="input" index="2PTkhb" />
        <child id="1220375669566529925" name="output" index="2PTkiL" />
      </concept>
      <concept id="1220375669566347117" name="jetbrains.mps.lang.editor.diagram.structure.ConnectionEndBLQuery" flags="ng" index="2PTV9p">
        <child id="2915596886892604954" name="targetNode" index="3B0qBL" />
      </concept>
      <concept id="8570854907290423690" name="jetbrains.mps.lang.editor.diagram.structure.DiagramElementsCreation" flags="ng" index="3cx5EE">
        <child id="8570854907290527457" name="handler" index="3cxIR1" />
      </concept>
      <concept id="8570854907290527479" name="jetbrains.mps.lang.editor.diagram.structure.DiagramElementCreationHandler" flags="ig" index="3cxIRn" />
      <concept id="8570854907290717922" name="jetbrains.mps.lang.editor.diagram.structure.XFunctionParameter" flags="ng" index="3cyWn2" />
      <concept id="8570854907290717911" name="jetbrains.mps.lang.editor.diagram.structure.YFunctionParameter" flags="ng" index="3cyWnR" />
      <concept id="8570854907290717918" name="jetbrains.mps.lang.editor.diagram.structure.NodeFunctionParameter" flags="ng" index="3cyWnY" />
      <concept id="5422656561926747342" name="jetbrains.mps.lang.editor.diagram.structure.AttributedFigureReference" flags="ng" index="3FP96B">
        <reference id="5422656561931890753" name="figureAttribute" index="3FDhkC" />
      </concept>
      <concept id="939897302409170270" name="jetbrains.mps.lang.editor.diagram.structure.ToNodeFunctionParameter" flags="ng" index="3Iumb3" />
      <concept id="939897302409170265" name="jetbrains.mps.lang.editor.diagram.structure.FromNodeFunctionParameter" flags="ng" index="3Iumb4" />
      <concept id="939897302409084996" name="jetbrains.mps.lang.editor.diagram.structure.DiagramConnectorCreation" flags="ng" index="3IuyZp">
        <child id="939897302409084999" name="canCreate" index="3IuyZq" />
        <child id="939897302409114956" name="handler" index="3Iu_Fh" />
      </concept>
      <concept id="939897302409085052" name="jetbrains.mps.lang.editor.diagram.structure.DiagramConnectorCreationHandler" flags="ig" index="3IuyZx" />
      <concept id="939897302409110350" name="jetbrains.mps.lang.editor.diagram.structure.DiagramConnectorCanCreateHandler" flags="ig" index="3Iu$Nj" />
      <concept id="3229274890673749551" name="jetbrains.mps.lang.editor.diagram.structure.ThisEditorNodeExpression" flags="ng" index="1SoGT8" />
      <concept id="5355858557208817201" name="jetbrains.mps.lang.editor.diagram.structure.DiagramElementBLQuery" flags="ng" index="1VYjFa">
        <child id="5355858557208817241" name="query" index="1VYjEy" />
      </concept>
      <concept id="285670992217672837" name="jetbrains.mps.lang.editor.diagram.structure.PropertyArgument" flags="ng" index="3YcAj5">
        <reference id="285670992217689748" name="property" index="3Ycyrk" />
      </concept>
      <concept id="285670992213637367" name="jetbrains.mps.lang.editor.diagram.structure.BLQueryArgument" flags="ng" index="3Ys12R">
        <child id="285670992213637368" name="query" index="3Ys12S" />
      </concept>
      <concept id="285670992213637559" name="jetbrains.mps.lang.editor.diagram.structure.LinkArgument" flags="ng" index="3Ys17R">
        <reference id="285670992217679783" name="link" index="3Yc$ZB" />
      </concept>
      <concept id="285670992205972098" name="jetbrains.mps.lang.editor.diagram.structure.CellModel_DiagramPort" flags="sg" stub="730538219795610316" index="3YTeF2">
        <property id="285670992206001471" name="input" index="3YT9PZ" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1171305280644" name="jetbrains.mps.lang.smodel.structure.Node_GetDescendantsOperation" flags="nn" index="2Rf3mk" />
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1144100932627" name="jetbrains.mps.lang.smodel.structure.OperationParm_Inclusion" flags="ng" index="1xIGOp" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI">
        <reference id="1140138128738" name="concept" index="1PxNhF" />
        <child id="1140138123956" name="leftExpression" index="1PxMeX" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
      <concept id="1201792049884" name="jetbrains.mps.baseLanguage.collections.structure.TranslateOperation" flags="nn" index="3goQfb" />
    </language>
  </registry>
  <node concept="24kQdi" id="OL$4j01kYe">
    <ref role="1XX52x" to="qq7b:OL$4j00qWr" resolve="Project" />
    <node concept="2aJ2om" id="5$vcisXC1_e" role="CpUAK">
      <ref role="2$4xQ3" node="5$vcisXC0c4" resolve="Diagram" />
    </node>
    <node concept="2b3QIZ" id="6jCwnQ4$i5B" role="2wV5jI">
      <node concept="3IuyZp" id="5$vcisXAOjl" role="3Iu_Fc">
        <property role="TrG5h" value="CreateAConnection" />
        <ref role="mdGOV" to="qq7b:6jCwnQ4CuxY" resolve="PackageElement" />
        <node concept="3Iu$Nj" id="5$vcisXAOjm" role="3IuyZq">
          <node concept="3clFbS" id="5$vcisXAOjn" role="2VODD2">
            <node concept="3clFbF" id="5$vcisXAPo7" role="3cqZAp">
              <node concept="3clFbT" id="5$vcisXAPLE" role="3clFbG">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3IuyZx" id="5$vcisXAOjo" role="3Iu_Fh">
          <node concept="3clFbS" id="5$vcisXAOjp" role="2VODD2">
            <node concept="3clFbF" id="5$vcisXAPWe" role="3cqZAp">
              <node concept="2OqwBi" id="5$vcisXAREH" role="3clFbG">
                <node concept="2OqwBi" id="5$vcisXAQfO" role="2Oq$k0">
                  <node concept="1PxgMI" id="5$vcisXAQcB" role="2Oq$k0">
                    <ref role="1PxNhF" to="qq7b:OL$4j00zeX" resolve="Package" />
                    <node concept="3Iumb4" id="5$vcisXAQaM" role="1PxMeX" />
                  </node>
                  <node concept="3Tsc0h" id="5$vcisXAQY3" role="2OqNvi">
                    <ref role="3TtcxE" to="qq7b:5$vcisX$34z" />
                  </node>
                </node>
                <node concept="TSZUe" id="5$vcisXAUfr" role="2OqNvi">
                  <node concept="1PxgMI" id="5$vcisXAUyC" role="25WWJ7">
                    <ref role="1PxNhF" to="qq7b:OL$4j00zeX" resolve="Package" />
                    <node concept="3Iumb3" id="5$vcisXAUnO" role="1PxMeX" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="5$vcisXAOD7" role="mdGOx">
          <node concept="1SoGT8" id="5$vcisXAOBB" role="2Oq$k0" />
          <node concept="3Tsc0h" id="5$vcisXAP26" role="2OqNvi">
            <ref role="3TtcxE" to="qq7b:5$vcisXyyM4" />
          </node>
        </node>
      </node>
      <node concept="3cx5EE" id="6jCwnQ4_FRV" role="3cyXsl">
        <property role="TrG5h" value="CreatePackageElement" />
        <ref role="mdGOV" to="qq7b:6jCwnQ4CuxY" resolve="PackageElement" />
        <node concept="3cxIRn" id="6jCwnQ4_FRW" role="3cxIR1">
          <node concept="3clFbS" id="6jCwnQ4_FRX" role="2VODD2">
            <node concept="3clFbF" id="6jCwnQ4B5WL" role="3cqZAp">
              <node concept="37vLTI" id="6jCwnQ4B6Ah" role="3clFbG">
                <node concept="3cyWn2" id="6jCwnQ4B6Az" role="37vLTx" />
                <node concept="2OqwBi" id="6jCwnQ4B5YD" role="37vLTJ">
                  <node concept="3cyWnY" id="6jCwnQ4B5WK" role="2Oq$k0" />
                  <node concept="3TrcHB" id="6jCwnQ4CAMu" role="2OqNvi">
                    <ref role="3TsBF5" to="qq7b:6jCwnQ4_Q72" resolve="x" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6jCwnQ4B6Nt" role="3cqZAp">
              <node concept="37vLTI" id="6jCwnQ4B7t4" role="3clFbG">
                <node concept="3cyWnR" id="6jCwnQ4B7tm" role="37vLTx" />
                <node concept="2OqwBi" id="6jCwnQ4B6Po" role="37vLTJ">
                  <node concept="3cyWnY" id="6jCwnQ4B6Nr" role="2Oq$k0" />
                  <node concept="3TrcHB" id="6jCwnQ4CBci" role="2OqNvi">
                    <ref role="3TsBF5" to="qq7b:6jCwnQ4_Q74" resolve="y" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6jCwnQ4BcCV" role="3cqZAp">
              <node concept="37vLTI" id="6jCwnQ4BdiI" role="3clFbG">
                <node concept="3cmrfG" id="6jCwnQ4Bdj0" role="37vLTx">
                  <property role="3cmrfH" value="100" />
                </node>
                <node concept="2OqwBi" id="6jCwnQ4BcET" role="37vLTJ">
                  <node concept="3cyWnY" id="6jCwnQ4BcCT" role="2Oq$k0" />
                  <node concept="3TrcHB" id="6jCwnQ4CwyS" role="2OqNvi">
                    <ref role="3TsBF5" to="qq7b:6jCwnQ4_QI8" resolve="width" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="6jCwnQ4Bdkp" role="3cqZAp">
              <node concept="37vLTI" id="6jCwnQ4Bebs" role="3clFbG">
                <node concept="3cmrfG" id="6jCwnQ4BebI" role="37vLTx">
                  <property role="3cmrfH" value="40" />
                </node>
                <node concept="2OqwBi" id="6jCwnQ4Bdmq" role="37vLTJ">
                  <node concept="3cyWnY" id="6jCwnQ4Bdkn" role="2Oq$k0" />
                  <node concept="3TrcHB" id="6jCwnQ4CBA6" role="2OqNvi">
                    <ref role="3TsBF5" to="qq7b:6jCwnQ4_QIc" resolve="height" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="6jCwnQ4_FVG" role="mdGOx">
          <node concept="1SoGT8" id="6jCwnQ4_FTR" role="2Oq$k0" />
          <node concept="3Tsc0h" id="5$vcisXz1xt" role="2OqNvi">
            <ref role="3TtcxE" to="qq7b:5$vcisXyyM4" />
          </node>
        </node>
      </node>
      <node concept="1VYjFa" id="6jCwnQ4Cu2M" role="1VXmjR">
        <node concept="2OqwBi" id="6jCwnQ4Cu80" role="1VYjEy">
          <node concept="1SoGT8" id="6jCwnQ4Cu4W" role="2Oq$k0" />
          <node concept="2Rf3mk" id="5$vcisXG9N9" role="2OqNvi">
            <node concept="1xMEDy" id="5$vcisXG9Nb" role="1xVPHs">
              <node concept="chp4Y" id="5$vcisXG9O5" role="ri$Ld">
                <ref role="cht4Q" to="qq7b:OL$4j00zeX" resolve="Package" />
              </node>
            </node>
            <node concept="1xIGOp" id="5$vcisXHPBR" role="1xVPHs" />
          </node>
        </node>
      </node>
      <node concept="1VYjFa" id="5$vcisX_R65" role="1VXmjR">
        <node concept="2OqwBi" id="5$vcisXFv5b" role="1VYjEy">
          <node concept="2OqwBi" id="5$vcisX_Rb6" role="2Oq$k0">
            <node concept="1SoGT8" id="5$vcisX_R95" role="2Oq$k0" />
            <node concept="2Rf3mk" id="5$vcisXHPN7" role="2OqNvi">
              <node concept="1xMEDy" id="5$vcisXHPN9" role="1xVPHs">
                <node concept="chp4Y" id="5$vcisXHPWA" role="ri$Ld">
                  <ref role="cht4Q" to="qq7b:OL$4j00zeX" resolve="Package" />
                </node>
              </node>
              <node concept="1xIGOp" id="5$vcisXHQfu" role="1xVPHs" />
            </node>
          </node>
          <node concept="3goQfb" id="5$vcisXFxE$" role="2OqNvi">
            <node concept="1bVj0M" id="5$vcisXFxEA" role="23t8la">
              <node concept="3clFbS" id="5$vcisXFxEB" role="1bW5cS">
                <node concept="3clFbF" id="5$vcisXFxJZ" role="3cqZAp">
                  <node concept="2OqwBi" id="5$vcisXFxO7" role="3clFbG">
                    <node concept="37vLTw" id="5$vcisXFxJY" role="2Oq$k0">
                      <ref role="3cqZAo" node="5$vcisXFxEC" resolve="it" />
                    </node>
                    <node concept="3Tsc0h" id="5$vcisXFyhg" role="2OqNvi">
                      <ref role="3TtcxE" to="qq7b:5$vcisX$34z" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="Rh6nW" id="5$vcisXFxEC" role="1bW2Oz">
                <property role="TrG5h" value="it" />
                <node concept="2jxLKc" id="5$vcisXFxED" role="1tU5fm" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2p8riq" id="6nPrS2k1MKs" role="2qB1ji">
        <node concept="2qV3X7" id="6nPrS2k1MMG" role="2p8WK$">
          <ref role="2qTo_q" node="6jCwnQ4_FRV" resolve="CreatePackageElement" />
        </node>
        <node concept="2qV3X7" id="5$vcisXBnPn" role="2p8WK$">
          <ref role="2qTo_q" node="5$vcisXAOjl" resolve="CreateAConnection" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6jCwnQ4$lka">
    <ref role="1XX52x" to="qq7b:6jCwnQ4_tMK" resolve="Project2" />
    <node concept="3EZMnI" id="6jCwnQ4$lkh" role="2wV5jI">
      <node concept="l2Vlx" id="6jCwnQ4$lki" role="2iSdaV" />
      <node concept="3F0ifn" id="6jCwnQ4$lkl" role="3EZMnx">
        <property role="3F0ifm" value="I have embedded a diagram cell below this constant cell:" />
      </node>
      <node concept="2b3QIZ" id="6jCwnQ4$lkq" role="3EZMnx">
        <node concept="3cx5EE" id="6nPrS2k43lh" role="3cyXsl">
          <property role="TrG5h" value="CreatePackageElement" />
          <ref role="mdGOV" to="qq7b:6jCwnQ4CuxY" resolve="PackageElement" />
          <node concept="3cxIRn" id="6nPrS2k43li" role="3cxIR1">
            <node concept="3clFbS" id="6nPrS2k43lj" role="2VODD2">
              <node concept="3clFbF" id="6nPrS2k43lk" role="3cqZAp">
                <node concept="37vLTI" id="6nPrS2k43ll" role="3clFbG">
                  <node concept="3cyWn2" id="6nPrS2k43lm" role="37vLTx" />
                  <node concept="2OqwBi" id="6nPrS2k43ln" role="37vLTJ">
                    <node concept="3cyWnY" id="6nPrS2k43lo" role="2Oq$k0" />
                    <node concept="3TrcHB" id="6nPrS2k43lp" role="2OqNvi">
                      <ref role="3TsBF5" to="qq7b:6jCwnQ4_Q72" resolve="x" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="6nPrS2k43lq" role="3cqZAp">
                <node concept="37vLTI" id="6nPrS2k43lr" role="3clFbG">
                  <node concept="3cyWnR" id="6nPrS2k43ls" role="37vLTx" />
                  <node concept="2OqwBi" id="6nPrS2k43lt" role="37vLTJ">
                    <node concept="3cyWnY" id="6nPrS2k43lu" role="2Oq$k0" />
                    <node concept="3TrcHB" id="6nPrS2k43lv" role="2OqNvi">
                      <ref role="3TsBF5" to="qq7b:6jCwnQ4_Q74" resolve="y" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="6nPrS2k43lw" role="3cqZAp">
                <node concept="37vLTI" id="6nPrS2k43lx" role="3clFbG">
                  <node concept="3cmrfG" id="6nPrS2k43ly" role="37vLTx">
                    <property role="3cmrfH" value="100" />
                  </node>
                  <node concept="2OqwBi" id="6nPrS2k43lz" role="37vLTJ">
                    <node concept="3cyWnY" id="6nPrS2k43l$" role="2Oq$k0" />
                    <node concept="3TrcHB" id="6nPrS2k43l_" role="2OqNvi">
                      <ref role="3TsBF5" to="qq7b:6jCwnQ4_QI8" resolve="width" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="3clFbF" id="6nPrS2k43lA" role="3cqZAp">
                <node concept="37vLTI" id="6nPrS2k43lB" role="3clFbG">
                  <node concept="3cmrfG" id="6nPrS2k43lC" role="37vLTx">
                    <property role="3cmrfH" value="40" />
                  </node>
                  <node concept="2OqwBi" id="6nPrS2k43lD" role="37vLTJ">
                    <node concept="3cyWnY" id="6nPrS2k43lE" role="2Oq$k0" />
                    <node concept="3TrcHB" id="6nPrS2k43lF" role="2OqNvi">
                      <ref role="3TsBF5" to="qq7b:6jCwnQ4_QIc" resolve="height" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="6nPrS2k43lG" role="mdGOx">
            <node concept="1SoGT8" id="6nPrS2k43lH" role="2Oq$k0" />
            <node concept="3Tsc0h" id="6nPrS2k43lI" role="2OqNvi">
              <ref role="3TtcxE" to="qq7b:OL$4j00zf3" />
            </node>
          </node>
        </node>
        <node concept="1VYjFa" id="6jCwnQ4$BOq" role="1VXmjR">
          <node concept="2OqwBi" id="6jCwnQ4AoN2" role="1VYjEy">
            <node concept="1SoGT8" id="6jCwnQ4AoKb" role="2Oq$k0" />
            <node concept="3Tsc0h" id="6nPrS2k4ojA" role="2OqNvi">
              <ref role="3TtcxE" to="qq7b:OL$4j00zf3" />
            </node>
          </node>
        </node>
        <node concept="pVoyu" id="6jCwnQ4$lk$" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="2p8riq" id="6nPrS2k4yfM" role="2qB1ji">
          <node concept="2qV3X7" id="6nPrS2k4yhP" role="2p8WK$">
            <ref role="2qTo_q" node="6nPrS2k43lh" resolve="CreatePackageElement" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="6jCwnQ4$lkH" role="3EZMnx">
        <property role="3F0ifm" value="The diagram cell is above this text." />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6jCwnQ4_JP2">
    <ref role="1XX52x" to="qq7b:6jCwnQ4CuxY" resolve="PackageElement" />
    <node concept="9$NOg" id="6jCwnQ4_JP4" role="2wV5jI">
      <node concept="9_WKQ" id="6jCwnQ4_Q5p" role="9_WL3">
        <property role="TrG5h" value="POSITION_X" />
        <node concept="3YcAj5" id="6jCwnQ4E8nI" role="3YbGMt">
          <ref role="3Ycyrk" to="qq7b:6jCwnQ4_Q72" resolve="x" />
        </node>
      </node>
      <node concept="9_WKQ" id="6jCwnQ4_Q8o" role="9_WL3">
        <property role="TrG5h" value="POSITION_Y" />
        <node concept="3YcAj5" id="6jCwnQ4E8p2" role="3YbGMt">
          <ref role="3Ycyrk" to="qq7b:6jCwnQ4_Q74" resolve="y" />
        </node>
      </node>
      <node concept="9_WKQ" id="6jCwnQ4_Qbf" role="9_WL3">
        <property role="TrG5h" value="editable" />
        <node concept="3Ys12R" id="6jCwnQ4_Qdp" role="3YbGMt">
          <node concept="3clFbT" id="6jCwnQ4_Qf4" role="3Ys12S">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="9_WKQ" id="6jCwnQ4_QgX" role="9_WL3">
        <property role="TrG5h" value="figureHeight" />
        <node concept="3YcAj5" id="6jCwnQ4E8qm" role="3YbGMt">
          <ref role="3Ycyrk" to="qq7b:6jCwnQ4_QIc" resolve="height" />
        </node>
      </node>
      <node concept="9_WKQ" id="6jCwnQ4_QMx" role="9_WL3">
        <property role="TrG5h" value="figureWidth" />
        <node concept="3YcAj5" id="6jCwnQ4E8sy" role="3YbGMt">
          <ref role="3Ycyrk" to="qq7b:6jCwnQ4_QI8" resolve="width" />
        </node>
      </node>
      <node concept="9_WKQ" id="6jCwnQ4_QUO" role="9_WL3">
        <property role="TrG5h" value="nameText" />
        <node concept="3YcAj5" id="6jCwnQ4_R69" role="3YbGMt">
          <ref role="3Ycyrk" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
      <node concept="9_WKQ" id="6jCwnQ4_R9p" role="9_WL3">
        <property role="TrG5h" value="lineWidth" />
        <node concept="3Ys12R" id="6jCwnQ4_RdS" role="3YbGMt">
          <node concept="3cmrfG" id="6jCwnQ4_Rh5" role="3Ys12S">
            <property role="3cmrfH" value="4" />
          </node>
        </node>
      </node>
      <node concept="3FP96B" id="6jCwnQ4_Q5c" role="9$N8C">
        <ref role="3FDhkC" to="8tro:HEilRNywb_" />
      </node>
      <node concept="3Ys17R" id="6nPrS2k1q$7" role="zbHsl">
        <ref role="3Yc$ZB" to="qq7b:6jCwnQ4EePM" />
      </node>
      <node concept="3Ys17R" id="6jCwnQ4EhCL" role="zbHsC">
        <ref role="3Yc$ZB" to="qq7b:6jCwnQ4Eggu" />
      </node>
    </node>
    <node concept="3EZMnI" id="5$vcisX$$qv" role="6VMZX">
      <node concept="3F0A7n" id="5$vcisX$$qM" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="l2Vlx" id="5$vcisX$$qy" role="2iSdaV" />
    </node>
    <node concept="2aJ2om" id="5$vcisXC0IC" role="CpUAK">
      <ref role="2$4xQ3" node="5$vcisXC0c4" resolve="Diagram" />
    </node>
  </node>
  <node concept="24kQdi" id="6jCwnQ4CRjH">
    <ref role="1XX52x" to="qq7b:6jCwnQ4CRju" resolve="PackageConnector" />
    <node concept="2aJ2om" id="5$vcisXC0fo" role="CpUAK">
      <ref role="2$4xQ3" node="5$vcisXC0c4" resolve="Diagram" />
    </node>
    <node concept="2FuRD1" id="6jCwnQ4CRjJ" role="2wV5jI">
      <node concept="2PTV9p" id="6jCwnQ4CRBr" role="2PTkiL">
        <node concept="2OqwBi" id="6jCwnQ4CRD2" role="3B0qBL">
          <node concept="1SoGT8" id="6jCwnQ4CRBH" role="2Oq$k0" />
          <node concept="3TrEf2" id="6jCwnQ4CSad" role="2OqNvi">
            <ref role="3Tt5mk" to="qq7b:6jCwnQ4CRj_" />
          </node>
        </node>
      </node>
      <node concept="2PTV9p" id="6jCwnQ4D30W" role="2PTkhb">
        <node concept="2OqwBi" id="6jCwnQ4D32x" role="3B0qBL">
          <node concept="1SoGT8" id="6jCwnQ4D31c" role="2Oq$k0" />
          <node concept="3TrEf2" id="6jCwnQ4D3j1" role="2OqNvi">
            <ref role="3Tt5mk" to="qq7b:6jCwnQ4CRjv" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6nPrS2k1rly">
    <ref role="1XX52x" to="qq7b:6jCwnQ4CSaR" resolve="PackageInputPort" />
    <node concept="2aJ2om" id="5$vcisXC0IP" role="CpUAK">
      <ref role="2$4xQ3" node="5$vcisXC0c4" resolve="Diagram" />
    </node>
    <node concept="3YTeF2" id="5$vcisXEOX0" role="2wV5jI">
      <property role="3YT9PZ" value="true" />
    </node>
  </node>
  <node concept="KZc4b" id="6jCwnQ4$i6j">
    <property role="TrG5h" value="FIGURES" />
    <property role="KZc57" value="/Users/fac2003/Dropbox/MPS_Book/figures/" />
  </node>
  <node concept="24kQdi" id="5$vcisXuzmV">
    <ref role="1XX52x" to="qq7b:17Ut3LPgSfq" resolve="PackageRef" />
    <node concept="2aJ2om" id="5$vcisXC0LR" role="CpUAK">
      <ref role="2$4xQ3" node="5$vcisXC0c4" resolve="Diagram" />
    </node>
    <node concept="2FuRD1" id="5$vcisXuzp8" role="2wV5jI">
      <node concept="2PTV9p" id="5$vcisXuzpk" role="2PTkhb">
        <node concept="2OqwBi" id="5$vcisXuzqY" role="3B0qBL">
          <node concept="1SoGT8" id="5$vcisXuzpC" role="2Oq$k0" />
          <node concept="1mfA1w" id="5$vcisXyx5R" role="2OqNvi" />
        </node>
      </node>
      <node concept="2PTV9p" id="5$vcisXuzG1" role="2PTkiL">
        <node concept="2OqwBi" id="5$vcisXyxat" role="3B0qBL">
          <node concept="1SoGT8" id="5$vcisXuzGl" role="2Oq$k0" />
          <node concept="3TrEf2" id="5$vcisXyxr2" role="2OqNvi">
            <ref role="3Tt5mk" to="qq7b:17Ut3LPgSfr" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="5$vcisXC0as">
    <ref role="1XX52x" to="qq7b:OL$4j00qWr" resolve="Project" />
    <node concept="2aJ2om" id="5$vcisXC1Gk" role="CpUAK">
      <ref role="2$4xQ3" node="5$vcisXC0c1" resolve="Text" />
    </node>
    <node concept="3EZMnI" id="5$vcisXC0aC" role="2wV5jI">
      <node concept="l2Vlx" id="5$vcisXC0aD" role="2iSdaV" />
      <node concept="3F0ifn" id="5$vcisXC0aE" role="3EZMnx">
        <property role="3F0ifm" value="project" />
      </node>
      <node concept="3F0A7n" id="5$vcisXC0aF" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="5$vcisXC0aG" role="3EZMnx">
        <property role="3F0ifm" value="{" />
        <node concept="3mYdg7" id="5$vcisXC0aH" role="3F10Kt">
          <property role="1413C4" value="body-brace" />
        </node>
        <node concept="ljvvj" id="5$vcisXC0aI" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="5$vcisXC0aJ" role="3EZMnx">
        <node concept="l2Vlx" id="5$vcisXC0aK" role="2iSdaV" />
        <node concept="lj46D" id="5$vcisXC0aL" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3F0ifn" id="5$vcisXC0aM" role="3EZMnx">
          <property role="3F0ifm" value="packages" />
        </node>
        <node concept="3F0ifn" id="5$vcisXC0aN" role="3EZMnx">
          <property role="3F0ifm" value=":" />
          <node concept="11L4FC" id="5$vcisXC0aO" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="ljvvj" id="5$vcisXC0aP" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F2HdR" id="5$vcisXC0aQ" role="3EZMnx">
          <ref role="1NtTu8" to="qq7b:5$vcisXyyM4" />
          <node concept="l2Vlx" id="5$vcisXC0aR" role="2czzBx" />
          <node concept="pj6Ft" id="5$vcisXC0aS" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="5$vcisXC0aT" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="ljvvj" id="5$vcisXC0aU" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="5$vcisXC0aV" role="3EZMnx">
        <property role="3F0ifm" value="}" />
        <node concept="3mYdg7" id="5$vcisXC0aW" role="3F10Kt">
          <property role="1413C4" value="body-brace" />
        </node>
      </node>
    </node>
  </node>
  <node concept="2ABfQD" id="5$vcisXC0c0">
    <property role="TrG5h" value="Textual" />
    <node concept="2BsEeg" id="5$vcisXC0c1" role="2ABdcP">
      <property role="2gpH_U" value="true" />
      <property role="TrG5h" value="Text" />
      <property role="2BUmq6" value="Text" />
    </node>
    <node concept="2BsEeg" id="5$vcisXC0c4" role="2ABdcP">
      <property role="2gpH_U" value="true" />
      <property role="TrG5h" value="Diagram" />
      <property role="2BUmq6" value="Diagram" />
    </node>
  </node>
  <node concept="24kQdi" id="5$vcisXC1GB">
    <ref role="1XX52x" to="qq7b:OL$4j00zeX" resolve="Package" />
    <node concept="3EZMnI" id="5$vcisXC1GD" role="2wV5jI">
      <node concept="l2Vlx" id="5$vcisXC1GE" role="2iSdaV" />
      <node concept="3F0ifn" id="5$vcisXC1GF" role="3EZMnx">
        <property role="3F0ifm" value="package" />
      </node>
      <node concept="3F0A7n" id="5$vcisXC1GG" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="5$vcisXC1GH" role="3EZMnx">
        <property role="3F0ifm" value="{" />
        <node concept="3mYdg7" id="5$vcisXC1GI" role="3F10Kt">
          <property role="1413C4" value="body-brace" />
        </node>
        <node concept="ljvvj" id="5$vcisXC1GJ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="5$vcisXC1GK" role="3EZMnx">
        <node concept="l2Vlx" id="5$vcisXC1GL" role="2iSdaV" />
        <node concept="lj46D" id="5$vcisXC1GM" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3F0ifn" id="5$vcisXC1GN" role="3EZMnx">
          <property role="3F0ifm" value="contains" />
        </node>
        <node concept="3F0ifn" id="5$vcisXC1GO" role="3EZMnx">
          <property role="3F0ifm" value=":" />
          <node concept="11L4FC" id="5$vcisXC1GP" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="ljvvj" id="5$vcisXC1GQ" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F2HdR" id="5$vcisXC1GR" role="3EZMnx">
          <ref role="1NtTu8" to="qq7b:5$vcisX$34z" />
          <node concept="l2Vlx" id="5$vcisXC1GS" role="2czzBx" />
          <node concept="pj6Ft" id="5$vcisXC1GT" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="5$vcisXC1GU" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="ljvvj" id="5$vcisXC1GV" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="5$vcisXC1GW" role="3EZMnx">
        <property role="3F0ifm" value="}" />
        <node concept="3mYdg7" id="5$vcisXC1GX" role="3F10Kt">
          <property role="1413C4" value="body-brace" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="5$vcisXEOWI">
    <ref role="1XX52x" to="qq7b:5$vcisXEfux" resolve="PackageOutputPort" />
    <node concept="3YTeF2" id="5$vcisXEOWW" role="2wV5jI" />
  </node>
</model>

