<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:2e3d7ff3-5fde-466c-9d84-5a929b65482a(org.campagnelab.book.diagrams.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ" />
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="OL$4j00qWr">
    <property role="TrG5h" value="Project" />
    <property role="34LRSv" value="project" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="5$vcisXyyM4" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="packages" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="OL$4j00zeX" resolve="Package" />
    </node>
    <node concept="PrWs8" id="OL$4j00zfg" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="OL$4j00zeX">
    <property role="TrG5h" value="Package" />
    <property role="34LRSv" value="package" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="5$vcisX$34z" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="contains" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="OL$4j00zeX" resolve="Package" />
    </node>
    <node concept="1TJgyj" id="5$vcisXEOWt" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="inputs" />
      <property role="20lbJX" value="1..n" />
      <ref role="20lvS9" node="6jCwnQ4CSaR" resolve="PackageInputPort" />
    </node>
    <node concept="1TJgyj" id="5$vcisXEOWy" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="outputs" />
      <property role="20lbJX" value="1..n" />
      <ref role="20lvS9" node="5$vcisXEfux" resolve="PackageOutputPort" />
    </node>
    <node concept="PrWs8" id="OL$4j00zeY" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="PlHQZ" id="OL$4j00zf2">
    <property role="TrG5h" value="HasPackages" />
    <node concept="1TJgyj" id="OL$4j00zf3" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="packages" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="OL$4j00zeX" resolve="Package" />
    </node>
  </node>
  <node concept="1TIwiD" id="17Ut3LPgSfq">
    <property role="TrG5h" value="PackageRef" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="17Ut3LPgSfr" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="package" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="OL$4j00zeX" resolve="Package" />
    </node>
  </node>
  <node concept="1TIwiD" id="6jCwnQ4_tMK">
    <property role="TrG5h" value="Project2" />
    <property role="34LRSv" value="project2" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="6jCwnQ4_tML" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="pack" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="17Ut3LPgSfq" resolve="PackageRef" />
    </node>
    <node concept="PrWs8" id="6jCwnQ4_tMM" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="6jCwnQ4_tMN" role="PzmwI">
      <ref role="PrY4T" node="OL$4j00zf2" resolve="HasPackages" />
    </node>
  </node>
  <node concept="1TIwiD" id="6jCwnQ4CuxY">
    <property role="TrG5h" value="PackageElement" />
    <ref role="1TJDcQ" node="OL$4j00zeX" resolve="Package" />
    <node concept="1TJgyj" id="6jCwnQ4EePM" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="inputPorts" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="6jCwnQ4CSaR" resolve="PackageInputPort" />
    </node>
    <node concept="1TJgyj" id="6jCwnQ4Eggu" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="outputPorts" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="6jCwnQ4CSaR" resolve="PackageInputPort" />
    </node>
    <node concept="1TJgyi" id="6jCwnQ4_Q72" role="1TKVEl">
      <property role="TrG5h" value="x" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="6jCwnQ4_Q74" role="1TKVEl">
      <property role="TrG5h" value="y" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="6jCwnQ4_QI8" role="1TKVEl">
      <property role="TrG5h" value="width" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="6jCwnQ4_QIc" role="1TKVEl">
      <property role="TrG5h" value="height" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="6jCwnQ4CRju">
    <property role="TrG5h" value="PackageConnector" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="6jCwnQ4CRjv" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="source" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="OL$4j00zf2" resolve="HasPackages" />
    </node>
    <node concept="1TJgyj" id="6jCwnQ4CRj_" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="destination" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="OL$4j00zf2" resolve="HasPackages" />
    </node>
  </node>
  <node concept="1TIwiD" id="6jCwnQ4CSaR">
    <property role="TrG5h" value="PackageInputPort" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="5$vcisXEfux">
    <property role="TrG5h" value="PackageOutputPort" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
</model>

