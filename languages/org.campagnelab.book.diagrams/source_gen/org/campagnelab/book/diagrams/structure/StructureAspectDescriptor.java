package org.campagnelab.book.diagrams.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.BaseStructureAspectDescriptor;
import jetbrains.mps.smodel.runtime.ConceptDescriptor;
import jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;
import java.util.Collection;
import java.util.Arrays;
import org.jetbrains.annotations.Nullable;

public class StructureAspectDescriptor extends BaseStructureAspectDescriptor {

  /*package*/ final ConceptDescriptor myConceptHasPackages = new ConceptDescriptorBuilder("org.campagnelab.book.diagrams.structure.HasPackages", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0xd319044c00233c2L)).interface_().childDescriptors(new ConceptDescriptorBuilder.Link(950699621305430979L, "packages", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0xd319044c00233bdL), true, true, false)).children(new String[]{"packages"}, new boolean[]{true}).create();
  /*package*/ final ConceptDescriptor myConceptPackage = new ConceptDescriptorBuilder("org.campagnelab.book.diagrams.structure.Package", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0xd319044c00233bdL)).super_("jetbrains.mps.lang.core.structure.BaseConcept").super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept", "jetbrains.mps.lang.core.structure.INamedConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL), MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L)).childDescriptors(new ConceptDescriptorBuilder.Link(6421905638259962147L, "contains", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0xd319044c00233bdL), true, true, false), new ConceptDescriptorBuilder.Link(6421905638261739293L, "inputs", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0x64e8817d84a382b7L), false, true, false), new ConceptDescriptorBuilder.Link(6421905638261739298L, "outputs", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0x591f31273da8f7a1L), false, true, false)).children(new String[]{"contains", "inputs", "outputs"}, new boolean[]{true, true, true}).alias("package", "").create();
  /*package*/ final ConceptDescriptor myConceptPackageConnector = new ConceptDescriptorBuilder("org.campagnelab.book.diagrams.structure.PackageConnector", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0x64e8817d84a374deL)).super_("jetbrains.mps.lang.core.structure.BaseConcept").super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).childDescriptors(new ConceptDescriptorBuilder.Link(7271203974485865695L, "source", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0xd319044c00233c2L), false, false, false), new ConceptDescriptorBuilder.Link(7271203974485865701L, "destination", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0xd319044c00233c2L), false, false, false)).children(new String[]{"source", "destination"}, new boolean[]{false, false}).create();
  /*package*/ final ConceptDescriptor myConceptPackageElement = new ConceptDescriptorBuilder("org.campagnelab.book.diagrams.structure.PackageElement", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0x64e8817d84a1e87eL)).super_("org.campagnelab.book.diagrams.structure.Package").super_(MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0xd319044c00233bdL)).parents("org.campagnelab.book.diagrams.structure.Package").parentIds(MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0xd319044c00233bdL)).propertyDescriptors(new ConceptDescriptorBuilder.Prop(7271203974485074370L, "x"), new ConceptDescriptorBuilder.Prop(7271203974485074372L, "y"), new ConceptDescriptorBuilder.Prop(7271203974485076872L, "width"), new ConceptDescriptorBuilder.Prop(7271203974485076876L, "height")).properties("x", "y", "width", "height").childDescriptors(new ConceptDescriptorBuilder.Link(7271203974486224242L, "inputPorts", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0x64e8817d84a382b7L), true, true, false), new ConceptDescriptorBuilder.Link(7271203974486230046L, "outputPorts", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0x64e8817d84a382b7L), true, true, false)).children(new String[]{"inputPorts", "outputPorts"}, new boolean[]{true, true}).create();
  /*package*/ final ConceptDescriptor myConceptPackageInputPort = new ConceptDescriptorBuilder("org.campagnelab.book.diagrams.structure.PackageInputPort", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0x64e8817d84a382b7L)).super_("jetbrains.mps.lang.core.structure.BaseConcept").super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).create();
  /*package*/ final ConceptDescriptor myConceptPackageOutputPort = new ConceptDescriptorBuilder("org.campagnelab.book.diagrams.structure.PackageOutputPort", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0x591f31273da8f7a1L)).super_("jetbrains.mps.lang.core.structure.BaseConcept").super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).create();
  /*package*/ final ConceptDescriptor myConceptPackageRef = new ConceptDescriptorBuilder("org.campagnelab.book.diagrams.structure.PackageRef", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0x11fa743c754383daL)).super_("jetbrains.mps.lang.core.structure.BaseConcept").super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).referenceDescriptors(new ConceptDescriptorBuilder.Ref(1295475645836657627L, "package", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0xd319044c00233bdL), false)).references("package").create();
  /*package*/ final ConceptDescriptor myConceptProject = new ConceptDescriptorBuilder("org.campagnelab.book.diagrams.structure.Project", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0xd319044c001af1bL)).super_("jetbrains.mps.lang.core.structure.BaseConcept").super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept", "jetbrains.mps.lang.core.structure.INamedConcept").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL), MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L)).childDescriptors(new ConceptDescriptorBuilder.Link(6421905638259567748L, "packages", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0xd319044c00233bdL), true, true, false)).children(new String[]{"packages"}, new boolean[]{true}).alias("project", "").create();
  /*package*/ final ConceptDescriptor myConceptProject2 = new ConceptDescriptorBuilder("org.campagnelab.book.diagrams.structure.Project2", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0x64e8817d8495dcb0L)).super_("jetbrains.mps.lang.core.structure.BaseConcept").super_(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL)).parents("jetbrains.mps.lang.core.structure.BaseConcept", "jetbrains.mps.lang.core.structure.INamedConcept", "org.campagnelab.book.diagrams.structure.HasPackages").parentIds(MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x10802efe25aL), MetaIdFactory.conceptId(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L), MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0xd319044c00233c2L)).childDescriptors(new ConceptDescriptorBuilder.Link(7271203974484974769L, "pack", MetaIdFactory.conceptId(0xd5c1e0d2588348fdL, 0xb91eeedaeb4e5156L, 0x11fa743c754383daL), true, true, false)).children(new String[]{"pack"}, new boolean[]{true}).alias("project2", "").create();

  @Override
  public Collection<ConceptDescriptor> getDescriptors() {
    return Arrays.asList(myConceptHasPackages, myConceptPackage, myConceptPackageConnector, myConceptPackageElement, myConceptPackageInputPort, myConceptPackageOutputPort, myConceptPackageRef, myConceptProject, myConceptProject2);
  }

  @Override
  @Nullable
  public ConceptDescriptor getDescriptor(String conceptFqName) {
    switch (Arrays.binarySearch(stringSwitchCases_1htk8d_a0a0n, conceptFqName)) {
      case 0:
        return myConceptHasPackages;
      case 1:
        return myConceptPackage;
      case 2:
        return myConceptPackageConnector;
      case 3:
        return myConceptPackageElement;
      case 4:
        return myConceptPackageInputPort;
      case 5:
        return myConceptPackageOutputPort;
      case 6:
        return myConceptPackageRef;
      case 7:
        return myConceptProject;
      case 8:
        return myConceptProject2;
      default:
        return null;
    }
  }
  private static String[] stringSwitchCases_1htk8d_a0a0n = new String[]{"org.campagnelab.book.diagrams.structure.HasPackages", "org.campagnelab.book.diagrams.structure.Package", "org.campagnelab.book.diagrams.structure.PackageConnector", "org.campagnelab.book.diagrams.structure.PackageElement", "org.campagnelab.book.diagrams.structure.PackageInputPort", "org.campagnelab.book.diagrams.structure.PackageOutputPort", "org.campagnelab.book.diagrams.structure.PackageRef", "org.campagnelab.book.diagrams.structure.Project", "org.campagnelab.book.diagrams.structure.Project2"};
}
