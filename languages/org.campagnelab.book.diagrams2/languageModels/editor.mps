<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:b0687fdd-c3fc-41d3-b902-2b44d5f00728(org.campagnelab.book.diagrams2.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="-1" />
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="-1" />
    <use id="ed6d7656-532c-4bc2-81d1-af945aeb8280" name="jetbrains.mps.baseLanguage.blTypes" version="-1" />
    <use id="443f4c36-fcf5-4eb6-9500-8d06ed259e3e" name="jetbrains.mps.baseLanguage.classifiers" version="-1" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="-1" />
    <use id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts" version="-1" />
    <use id="9ded098b-ad6a-4657-bfd9-48636cfe8bc3" name="jetbrains.mps.lang.traceable" version="-1" />
    <use id="6106f611-7a74-42d1-80de-edc5c602bfd1" name="jetbrains.mps.lang.editor.diagram" version="-1" />
    <use id="d7722d50-4b93-4c3a-ae06-1903d05f95a7" name="jetbrains.mps.lang.editor.figures" version="-1" />
    <use id="93bc01ac-08ca-4f11-9c7d-614d04055dfb" name="org.campagnelab.mps.editor2pdf" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="8tro" ref="r:257a7f19-40a4-4037-a93b-ce1b638af281(jetbrains.mps.lang.editor.figures.library)" />
    <import index="75qg" ref="r:5abdd903-598a-4f28-afd7-38c1105ed622(org.campagnelab.book.diagrams2.structure)" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="k7g3" ref="f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.util(JDK/java.util@java_stub)" implicit="true" />
  </imports>
  <registry>
    <language id="93bc01ac-08ca-4f11-9c7d-614d04055dfb" name="org.campagnelab.mps.editor2pdf">
      <concept id="893392931327129896" name="org.campagnelab.mps.editor2pdf.structure.DiagramOutputDirectory" flags="ng" index="KZc4b">
        <property id="893392931327129956" name="path" index="KZc57" />
      </concept>
      <concept id="8751972264247112684" name="org.campagnelab.mps.editor2pdf.structure.EditorAnnotation" flags="ng" index="3ZW7eb">
        <property id="5378718574870043633" name="outputFormat" index="2ripvU" />
        <reference id="893392931327136863" name="outputTo" index="KZaLW" />
      </concept>
    </language>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
      <concept id="6329021646629175143" name="jetbrains.mps.baseLanguage.structure.StatementCommentPart" flags="nn" index="3SKWN0">
        <child id="6329021646629175144" name="commentedStatement" index="3SKWNf" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="6106f611-7a74-42d1-80de-edc5c602bfd1" name="jetbrains.mps.lang.editor.diagram">
      <concept id="1094405431463454433" name="jetbrains.mps.lang.editor.diagram.structure.CellModel_DiagramNode" flags="sg" stub="730538219795610279" index="9$NOg">
        <child id="1094405431463455193" name="figure" index="9$N8C" />
        <child id="1094405431463761842" name="parameters" index="9_WL3" />
        <child id="2084788800269090635" name="inputPort" index="zbHsl" />
        <child id="2084788800269090678" name="outputPort" index="zbHsC" />
      </concept>
      <concept id="1094405431463761863" name="jetbrains.mps.lang.editor.diagram.structure.FigureParameterMapping" flags="ng" index="9_WKQ">
        <child id="285670992218957021" name="argument" index="3YbGMt" />
      </concept>
      <concept id="6306886970791033847" name="jetbrains.mps.lang.editor.diagram.structure.CellModel_Diagram" flags="sg" stub="730538219795567478" index="2b3QIZ">
        <child id="8570854907290721333" name="elementsCreation" index="3cyXsl" />
        <child id="939897302409114961" name="connectorCreation" index="3Iu_Fc" />
        <child id="5355858557208539148" name="diagramElements" index="1VXmjR" />
      </concept>
      <concept id="1301388602725986966" name="jetbrains.mps.lang.editor.diagram.structure.AbstractDiagramCreation" flags="ng" index="mdwis">
        <reference id="1301388602726005553" name="concept" index="mdGOV" />
        <child id="1301388602726005547" name="query" index="mdGOx" />
      </concept>
      <concept id="6382742553261733065" name="jetbrains.mps.lang.editor.diagram.structure.CellModel_DiagramConnector" flags="sg" stub="730538219795610242" index="2FuRD1">
        <child id="1220375669566529919" name="input" index="2PTkhb" />
        <child id="1220375669566529925" name="output" index="2PTkiL" />
      </concept>
      <concept id="1220375669566347117" name="jetbrains.mps.lang.editor.diagram.structure.ConnectionEndBLQuery" flags="ng" index="2PTV9p">
        <child id="1220375669566421348" name="pointID" index="2PTDLg" />
        <child id="2915596886892604954" name="targetNode" index="3B0qBL" />
      </concept>
      <concept id="8570854907290423690" name="jetbrains.mps.lang.editor.diagram.structure.DiagramElementsCreation" flags="ng" index="3cx5EE">
        <child id="8570854907290527457" name="handler" index="3cxIR1" />
      </concept>
      <concept id="8570854907290527479" name="jetbrains.mps.lang.editor.diagram.structure.DiagramElementCreationHandler" flags="ig" index="3cxIRn" />
      <concept id="8570854907290717922" name="jetbrains.mps.lang.editor.diagram.structure.XFunctionParameter" flags="ng" index="3cyWn2" />
      <concept id="8570854907290717911" name="jetbrains.mps.lang.editor.diagram.structure.YFunctionParameter" flags="ng" index="3cyWnR" />
      <concept id="8570854907290717918" name="jetbrains.mps.lang.editor.diagram.structure.NodeFunctionParameter" flags="ng" index="3cyWnY" />
      <concept id="5422656561926747342" name="jetbrains.mps.lang.editor.diagram.structure.AttributedFigureReference" flags="ng" index="3FP96B">
        <reference id="5422656561931890753" name="figureAttribute" index="3FDhkC" />
      </concept>
      <concept id="939897302409170270" name="jetbrains.mps.lang.editor.diagram.structure.ToNodeFunctionParameter" flags="ng" index="3Iumb3" />
      <concept id="939897302409170265" name="jetbrains.mps.lang.editor.diagram.structure.FromNodeFunctionParameter" flags="ng" index="3Iumb4" />
      <concept id="939897302409084996" name="jetbrains.mps.lang.editor.diagram.structure.DiagramConnectorCreation" flags="ng" index="3IuyZp">
        <child id="939897302409084999" name="canCreate" index="3IuyZq" />
        <child id="939897302409114956" name="handler" index="3Iu_Fh" />
      </concept>
      <concept id="939897302409085052" name="jetbrains.mps.lang.editor.diagram.structure.DiagramConnectorCreationHandler" flags="ig" index="3IuyZx" />
      <concept id="939897302409110350" name="jetbrains.mps.lang.editor.diagram.structure.DiagramConnectorCanCreateHandler" flags="ig" index="3Iu$Nj" />
      <concept id="3229274890673749551" name="jetbrains.mps.lang.editor.diagram.structure.ThisEditorNodeExpression" flags="ng" index="1SoGT8" />
      <concept id="5355858557208817201" name="jetbrains.mps.lang.editor.diagram.structure.DiagramElementBLQuery" flags="ng" index="1VYjFa">
        <child id="5355858557208817241" name="query" index="1VYjEy" />
      </concept>
      <concept id="285670992217672837" name="jetbrains.mps.lang.editor.diagram.structure.PropertyArgument" flags="ng" index="3YcAj5">
        <reference id="285670992217689748" name="property" index="3Ycyrk" />
      </concept>
      <concept id="285670992213637367" name="jetbrains.mps.lang.editor.diagram.structure.BLQueryArgument" flags="ng" index="3Ys12R">
        <child id="285670992213637368" name="query" index="3Ys12S" />
      </concept>
      <concept id="285670992213637559" name="jetbrains.mps.lang.editor.diagram.structure.LinkArgument" flags="ng" index="3Ys17R">
        <reference id="285670992217679783" name="link" index="3Yc$ZB" />
      </concept>
      <concept id="285670992205972098" name="jetbrains.mps.lang.editor.diagram.structure.CellModel_DiagramPort" flags="sg" stub="730538219795610316" index="3YTeF2">
        <property id="285670992206001471" name="input" index="3YT9PZ" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179168000618" name="jetbrains.mps.lang.smodel.structure.Node_GetIndexInParentOperation" flags="nn" index="2bSWHS" />
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1138411891628" name="jetbrains.mps.lang.smodel.structure.SNodeOperation" flags="nn" index="eCIE_">
        <child id="1144104376918" name="parameter" index="1xVPHs" />
      </concept>
      <concept id="1171407110247" name="jetbrains.mps.lang.smodel.structure.Node_GetAncestorOperation" flags="nn" index="2Xjw5R" />
      <concept id="1144101972840" name="jetbrains.mps.lang.smodel.structure.OperationParm_Concept" flags="ng" index="1xMEDy">
        <child id="1207343664468" name="conceptArgument" index="ri$Ld" />
      </concept>
      <concept id="1180636770613" name="jetbrains.mps.lang.smodel.structure.SNodeCreator" flags="nn" index="3zrR0B">
        <child id="1180636770616" name="createdType" index="3zrR0E" />
      </concept>
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
      <concept id="1225727723840" name="jetbrains.mps.baseLanguage.collections.structure.FindFirstOperation" flags="nn" index="1z4cxt" />
    </language>
  </registry>
  <node concept="24kQdi" id="5$vcisXIvqu">
    <ref role="1XX52x" to="75qg:5$vcisXIvps" resolve="Node" />
    <node concept="9$NOg" id="5$vcisXIvrr" role="2wV5jI">
      <node concept="9_WKQ" id="5$vcisXIv$E" role="9_WL3">
        <property role="TrG5h" value="POSITION_X" />
        <node concept="3YcAj5" id="5$vcisXIvCc" role="3YbGMt">
          <ref role="3Ycyrk" to="75qg:5$vcisXIvBC" resolve="x" />
        </node>
      </node>
      <node concept="9_WKQ" id="5$vcisXIvCT" role="9_WL3">
        <property role="TrG5h" value="POSITION_Y" />
        <node concept="3YcAj5" id="5$vcisXIvEz" role="3YbGMt">
          <ref role="3Ycyrk" to="75qg:5$vcisXIvBE" resolve="y" />
        </node>
      </node>
      <node concept="9_WKQ" id="5$vcisXIvFO" role="9_WL3">
        <property role="TrG5h" value="nameText" />
        <node concept="3YcAj5" id="5$vcisXIvKM" role="3YbGMt">
          <ref role="3Ycyrk" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
      <node concept="9_WKQ" id="5$vcisXIvMt" role="9_WL3">
        <property role="TrG5h" value="editable" />
        <node concept="3Ys12R" id="5$vcisXIvOU" role="3YbGMt">
          <node concept="3clFbT" id="5$vcisXIvR0" role="3Ys12S">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="3FP96B" id="5$vcisXIv$u" role="9$N8C">
        <ref role="3FDhkC" to="8tro:HEilRNywb_" />
      </node>
      <node concept="3Ys17R" id="2dy6Wl6TgYc" role="zbHsl">
        <ref role="3Yc$ZB" to="75qg:5$vcisXIvTk" />
      </node>
      <node concept="3Ys17R" id="2dy6Wl6TGyv" role="zbHsC">
        <ref role="3Yc$ZB" to="75qg:5$vcisXIvTu" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="2dy6Wl6THyR">
    <ref role="1XX52x" to="75qg:2dy6Wl6THg9" resolve="Project" />
    <node concept="2b3QIZ" id="2dy6Wl6TH$P" role="2wV5jI">
      <node concept="3IuyZp" id="2dy6Wl6VZ2Q" role="3Iu_Fc">
        <property role="TrG5h" value="CreateEdge" />
        <ref role="mdGOV" to="75qg:5$vcisXIvqk" resolve="Edge" />
        <node concept="3Iu$Nj" id="2dy6Wl6VZ2R" role="3IuyZq">
          <node concept="3clFbS" id="2dy6Wl6VZ2S" role="2VODD2">
            <node concept="3cpWs6" id="2dy6Wl70bqd" role="3cqZAp">
              <node concept="3clFbT" id="2dy6Wl70byu" role="3cqZAk">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3IuyZx" id="2dy6Wl6VZ2T" role="3Iu_Fh">
          <node concept="3clFbS" id="2dy6Wl6VZ2U" role="2VODD2">
            <node concept="3clFbF" id="2dy6Wl6W0Pv" role="3cqZAp">
              <node concept="37vLTI" id="2dy6Wl6W192" role="3clFbG">
                <node concept="2OqwBi" id="2dy6Wl70TH6" role="37vLTx">
                  <node concept="3Iumb4" id="2dy6Wl6W19U" role="2Oq$k0" />
                  <node concept="2Xjw5R" id="2dy6Wl70U3r" role="2OqNvi">
                    <node concept="1xMEDy" id="2dy6Wl70U3t" role="1xVPHs">
                      <node concept="chp4Y" id="2dy6Wl70U6v" role="ri$Ld">
                        <ref role="cht4Q" to="75qg:5$vcisXIvps" resolve="Node" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2dy6Wl6W0QF" role="37vLTJ">
                  <node concept="3cyWnY" id="2dy6Wl6W0Pu" role="2Oq$k0" />
                  <node concept="3TrEf2" id="2dy6Wl6W0Z3" role="2OqNvi">
                    <ref role="3Tt5mk" to="75qg:5$vcisXIvql" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2dy6Wl6W1ef" role="3cqZAp">
              <node concept="37vLTI" id="2dy6Wl6W1Fx" role="3clFbG">
                <node concept="2OqwBi" id="2dy6Wl70UbH" role="37vLTx">
                  <node concept="3Iumb3" id="2dy6Wl6W1FX" role="2Oq$k0" />
                  <node concept="2Xjw5R" id="2dy6Wl70UqJ" role="2OqNvi">
                    <node concept="1xMEDy" id="2dy6Wl70UqL" role="1xVPHs">
                      <node concept="chp4Y" id="2dy6Wl70UtN" role="ri$Ld">
                        <ref role="cht4Q" to="75qg:5$vcisXIvps" resolve="Node" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="2OqwBi" id="2dy6Wl6W1g1" role="37vLTJ">
                  <node concept="3cyWnY" id="2dy6Wl6W1ed" role="2Oq$k0" />
                  <node concept="3TrEf2" id="2dy6Wl6W1xp" role="2OqNvi">
                    <ref role="3Tt5mk" to="75qg:5$vcisXIvqn" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3SKdUt" id="4Q9edAsbhm3" role="3cqZAp">
              <node concept="3SKWN0" id="4Q9edAsbhm4" role="3SKWNk">
                <node concept="3clFbF" id="2dy6Wl6Wif2" role="3SKWNf">
                  <node concept="2OqwBi" id="2dy6Wl6Wjjw" role="3clFbG">
                    <node concept="2OqwBi" id="2dy6Wl6Wi_k" role="2Oq$k0">
                      <node concept="2OqwBi" id="2dy6Wl6Wih3" role="2Oq$k0">
                        <node concept="3cyWnY" id="2dy6Wl6Wif0" role="2Oq$k0" />
                        <node concept="2Xjw5R" id="2dy6Wl6Wiyx" role="2OqNvi">
                          <node concept="1xMEDy" id="2dy6Wl6Wiyz" role="1xVPHs">
                            <node concept="chp4Y" id="2dy6Wl6Wizj" role="ri$Ld">
                              <ref role="cht4Q" to="75qg:2dy6Wl6THg9" resolve="Project" />
                            </node>
                          </node>
                        </node>
                      </node>
                      <node concept="3Tsc0h" id="2dy6Wl6WiI$" role="2OqNvi">
                        <ref role="3TtcxE" to="75qg:2dy6Wl6THyJ" />
                      </node>
                    </node>
                    <node concept="TSZUe" id="2dy6Wl6Wlxw" role="2OqNvi">
                      <node concept="3cyWnY" id="2dy6Wl6Wl_Z" role="25WWJ7" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2dy6Wl71FBo" role="3cqZAp">
              <node concept="2OqwBi" id="2dy6Wl71HiM" role="3clFbG">
                <node concept="2OqwBi" id="2dy6Wl71G2Q" role="2Oq$k0">
                  <node concept="2OqwBi" id="2dy6Wl71FDZ" role="2Oq$k0">
                    <node concept="3cyWnY" id="2dy6Wl71FBm" role="2Oq$k0" />
                    <node concept="3TrEf2" id="2dy6Wl71FQ6" role="2OqNvi">
                      <ref role="3Tt5mk" to="75qg:5$vcisXIvql" />
                    </node>
                  </node>
                  <node concept="3Tsc0h" id="2dy6Wl71GEk" role="2OqNvi">
                    <ref role="3TtcxE" to="75qg:5$vcisXIvTu" />
                  </node>
                </node>
                <node concept="TSZUe" id="2dy6Wl71JAG" role="2OqNvi">
                  <node concept="2ShNRf" id="2dy6Wl71NB8" role="25WWJ7">
                    <node concept="3zrR0B" id="2dy6Wl71NPk" role="2ShVmc">
                      <node concept="3Tqbb2" id="2dy6Wl71NPm" role="3zrR0E">
                        <ref role="ehGHo" to="75qg:5$vcisXIvTr" resolve="OutputPort" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2dy6Wl71O18" role="3cqZAp">
              <node concept="2OqwBi" id="2dy6Wl71O19" role="3clFbG">
                <node concept="2OqwBi" id="2dy6Wl71O1a" role="2Oq$k0">
                  <node concept="2OqwBi" id="2dy6Wl71O1b" role="2Oq$k0">
                    <node concept="3cyWnY" id="2dy6Wl71O1c" role="2Oq$k0" />
                    <node concept="3TrEf2" id="2dy6Wl71OH_" role="2OqNvi">
                      <ref role="3Tt5mk" to="75qg:5$vcisXIvqn" />
                    </node>
                  </node>
                  <node concept="3Tsc0h" id="2dy6Wl71OTC" role="2OqNvi">
                    <ref role="3TtcxE" to="75qg:5$vcisXIvTk" />
                  </node>
                </node>
                <node concept="TSZUe" id="2dy6Wl71O1f" role="2OqNvi">
                  <node concept="2ShNRf" id="2dy6Wl71O1g" role="25WWJ7">
                    <node concept="3zrR0B" id="2dy6Wl71O1h" role="2ShVmc">
                      <node concept="3Tqbb2" id="2dy6Wl71O1i" role="3zrR0E">
                        <ref role="ehGHo" to="75qg:5$vcisXIvTo" resolve="InputPort" />
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2dy6Wl73M7s" role="3cqZAp">
              <node concept="37vLTI" id="2dy6Wl73N3b" role="3clFbG">
                <node concept="2OqwBi" id="2dy6Wl73Mdi" role="37vLTJ">
                  <node concept="3cyWnY" id="2dy6Wl73M7q" role="2Oq$k0" />
                  <node concept="3TrcHB" id="2dy6Wl73ME7" role="2OqNvi">
                    <ref role="3TsBF5" to="75qg:2dy6Wl72x0U" resolve="destinationPortIndex" />
                  </node>
                </node>
                <node concept="2OqwBi" id="2dy6Wl73QoI" role="37vLTx">
                  <node concept="3Iumb3" id="2dy6Wl73Qnx" role="2Oq$k0" />
                  <node concept="2bSWHS" id="2dy6Wl73QLg" role="2OqNvi" />
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2dy6Wl73RdV" role="3cqZAp">
              <node concept="37vLTI" id="2dy6Wl73SnM" role="3clFbG">
                <node concept="2OqwBi" id="2dy6Wl73SpH" role="37vLTx">
                  <node concept="3Iumb4" id="2dy6Wl73Sow" role="2Oq$k0" />
                  <node concept="2bSWHS" id="2dy6Wl73SwH" role="2OqNvi" />
                </node>
                <node concept="2OqwBi" id="2dy6Wl73RjZ" role="37vLTJ">
                  <node concept="3cyWnY" id="2dy6Wl73RdT" role="2Oq$k0" />
                  <node concept="3TrcHB" id="2dy6Wl73RYI" role="2OqNvi">
                    <ref role="3TsBF5" to="75qg:2dy6Wl72x0$" resolve="sourcePortIndex" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="2dy6Wl6VZcF" role="mdGOx">
          <node concept="1SoGT8" id="2dy6Wl6VZby" role="2Oq$k0" />
          <node concept="3Tsc0h" id="2dy6Wl6VZvs" role="2OqNvi">
            <ref role="3TtcxE" to="75qg:2dy6Wl6THyJ" />
          </node>
        </node>
        <node concept="3ZW7eb" id="2dy6Wl71hjS" role="lGtFl">
          <property role="2ripvU" value="1" />
          <property role="TrG5h" value="DiagramsCreateEdge" />
          <ref role="KZaLW" node="2dy6Wl71hwG" resolve="FIGURES" />
        </node>
      </node>
      <node concept="3cx5EE" id="2dy6Wl6VC6_" role="3cyXsl">
        <property role="TrG5h" value="ElementCreation" />
        <ref role="mdGOV" to="75qg:5$vcisXIvps" resolve="Node" />
        <node concept="3cxIRn" id="2dy6Wl6VC6A" role="3cxIR1">
          <node concept="3clFbS" id="2dy6Wl6VC6B" role="2VODD2">
            <node concept="3clFbF" id="2dy6Wl6VDUV" role="3cqZAp">
              <node concept="37vLTI" id="2dy6Wl6VEEs" role="3clFbG">
                <node concept="3cyWn2" id="2dy6Wl6VEEI" role="37vLTx" />
                <node concept="2OqwBi" id="2dy6Wl6VDWt" role="37vLTJ">
                  <node concept="3cyWnY" id="2dy6Wl6VDUU" role="2Oq$k0" />
                  <node concept="3TrcHB" id="2dy6Wl6VEgE" role="2OqNvi">
                    <ref role="3TsBF5" to="75qg:5$vcisXIvBC" resolve="x" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="2dy6Wl6VERu" role="3cqZAp">
              <node concept="37vLTI" id="2dy6Wl6VFtl" role="3clFbG">
                <node concept="3cyWnR" id="2dy6Wl6VFtB" role="37vLTx" />
                <node concept="2OqwBi" id="2dy6Wl6VET3" role="37vLTJ">
                  <node concept="3cyWnY" id="2dy6Wl6VERs" role="2Oq$k0" />
                  <node concept="3TrcHB" id="2dy6Wl6VF3$" role="2OqNvi">
                    <ref role="3TsBF5" to="75qg:5$vcisXIvBE" resolve="y" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="2dy6Wl6VDDm" role="mdGOx">
          <node concept="1SoGT8" id="2dy6Wl6VDCd" role="2Oq$k0" />
          <node concept="3Tsc0h" id="2dy6Wl6VDTI" role="2OqNvi">
            <ref role="3TtcxE" to="75qg:2dy6Wl6THyH" />
          </node>
        </node>
      </node>
      <node concept="1VYjFa" id="2dy6Wl6TH$V" role="1VXmjR">
        <node concept="2OqwBi" id="2dy6Wl6THAL" role="1VYjEy">
          <node concept="1SoGT8" id="2dy6Wl6TH_d" role="2Oq$k0" />
          <node concept="3Tsc0h" id="2dy6Wl6TS_v" role="2OqNvi">
            <ref role="3TtcxE" to="75qg:2dy6Wl6THyH" />
          </node>
        </node>
      </node>
      <node concept="1VYjFa" id="2dy6Wl6TSCZ" role="1VXmjR">
        <node concept="2OqwBi" id="2dy6Wl6TSFp" role="1VYjEy">
          <node concept="1SoGT8" id="2dy6Wl6TSDO" role="2Oq$k0" />
          <node concept="3Tsc0h" id="2dy6Wl6TSNU" role="2OqNvi">
            <ref role="3TtcxE" to="75qg:2dy6Wl6THyJ" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="2dy6Wl6V9PX">
    <ref role="1XX52x" to="75qg:5$vcisXIvqk" resolve="Edge" />
    <node concept="2FuRD1" id="2dy6Wl6V9PZ" role="2wV5jI">
      <node concept="2PTV9p" id="2dy6Wl6V9Q8" role="2PTkhb">
        <node concept="2OqwBi" id="2dy6Wl6V9RI" role="3B0qBL">
          <node concept="1SoGT8" id="2dy6Wl6V9Qq" role="2Oq$k0" />
          <node concept="3TrEf2" id="2dy6Wl6Va8e" role="2OqNvi">
            <ref role="3Tt5mk" to="75qg:5$vcisXIvql" />
          </node>
        </node>
        <node concept="2OqwBi" id="2dy6Wl6Zpw5" role="2PTDLg">
          <node concept="2OqwBi" id="2dy6Wl6Zo$w" role="2Oq$k0">
            <node concept="2OqwBi" id="2dy6Wl6Zo9k" role="2Oq$k0">
              <node concept="1SoGT8" id="2dy6Wl6Zo80" role="2Oq$k0" />
              <node concept="3TrEf2" id="2dy6Wl6ZoqO" role="2OqNvi">
                <ref role="3Tt5mk" to="75qg:5$vcisXIvql" />
              </node>
            </node>
            <node concept="3Tsc0h" id="2dy6Wl6ZoTi" role="2OqNvi">
              <ref role="3TtcxE" to="75qg:5$vcisXIvTu" />
            </node>
          </node>
          <node concept="1z4cxt" id="2dy6Wl73lRJ" role="2OqNvi">
            <node concept="1bVj0M" id="2dy6Wl73lRL" role="23t8la">
              <node concept="3clFbS" id="2dy6Wl73lRM" role="1bW5cS">
                <node concept="3clFbF" id="2dy6Wl73lRN" role="3cqZAp">
                  <node concept="3clFbC" id="2dy6Wl73lRO" role="3clFbG">
                    <node concept="2OqwBi" id="2dy6Wl73lRP" role="3uHU7w">
                      <node concept="1SoGT8" id="2dy6Wl73lRQ" role="2Oq$k0" />
                      <node concept="3TrcHB" id="2dy6Wl73lRR" role="2OqNvi">
                        <ref role="3TsBF5" to="75qg:2dy6Wl72x0$" resolve="sourcePortIndex" />
                      </node>
                    </node>
                    <node concept="2OqwBi" id="2dy6Wl73lRS" role="3uHU7B">
                      <node concept="37vLTw" id="2dy6Wl73lRT" role="2Oq$k0">
                        <ref role="3cqZAo" node="2dy6Wl73lRV" resolve="it" />
                      </node>
                      <node concept="2bSWHS" id="2dy6Wl73lRU" role="2OqNvi" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="Rh6nW" id="2dy6Wl73lRV" role="1bW2Oz">
                <property role="TrG5h" value="it" />
                <node concept="2jxLKc" id="2dy6Wl73lRW" role="1tU5fm" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="2PTV9p" id="2dy6Wl6Va9W" role="2PTkiL">
        <node concept="2OqwBi" id="2dy6Wl6Vabz" role="3B0qBL">
          <node concept="1SoGT8" id="2dy6Wl6Vaae" role="2Oq$k0" />
          <node concept="3TrEf2" id="2dy6Wl6Vas3" role="2OqNvi">
            <ref role="3Tt5mk" to="75qg:5$vcisXIvqn" />
          </node>
        </node>
        <node concept="2OqwBi" id="2dy6Wl6Zt62" role="2PTDLg">
          <node concept="2OqwBi" id="2dy6Wl6Zsak" role="2Oq$k0">
            <node concept="2OqwBi" id="2dy6Wl6YPwg" role="2Oq$k0">
              <node concept="1SoGT8" id="2dy6Wl6YPuW" role="2Oq$k0" />
              <node concept="3TrEf2" id="2dy6Wl6Zs0C" role="2OqNvi">
                <ref role="3Tt5mk" to="75qg:5$vcisXIvqn" />
              </node>
            </node>
            <node concept="3Tsc0h" id="2dy6Wl6Zsv6" role="2OqNvi">
              <ref role="3TtcxE" to="75qg:5$vcisXIvTk" />
            </node>
          </node>
          <node concept="liA8E" id="2dy6Wl73o9e" role="2OqNvi">
            <ref role="37wK5l" to="k7g3:~List.get(int):java.lang.Object" resolve="get" />
            <node concept="2OqwBi" id="2dy6Wl73ogQ" role="37wK5m">
              <node concept="1SoGT8" id="2dy6Wl73oe5" role="2Oq$k0" />
              <node concept="3TrcHB" id="2dy6Wl73oA7" role="2OqNvi">
                <ref role="3TsBF5" to="75qg:2dy6Wl72x0U" resolve="destinationPortIndex" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="2dy6Wl6VasY">
    <ref role="1XX52x" to="75qg:5$vcisXIvTo" resolve="InputPort" />
    <node concept="3YTeF2" id="2dy6Wl6Vat0" role="2wV5jI">
      <property role="3YT9PZ" value="true" />
    </node>
  </node>
  <node concept="24kQdi" id="2dy6Wl6Vat8">
    <ref role="1XX52x" to="75qg:5$vcisXIvTr" resolve="OutputPort" />
    <node concept="3YTeF2" id="2dy6Wl6Vata" role="2wV5jI" />
  </node>
  <node concept="KZc4b" id="2dy6Wl71hwG">
    <property role="TrG5h" value="FIGURES" />
    <property role="KZc57" value="/Users/fac2003/Dropbox/MPS_Book/figures/" />
  </node>
</model>

