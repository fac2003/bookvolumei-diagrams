package org.campagnelab.book.diagrams2.behavior;

/*Generated by MPS */

import jetbrains.mps.lang.core.behavior.BaseConcept_BehaviorDescriptor;

public class Project_BehaviorDescriptor extends BaseConcept_BehaviorDescriptor {
  public Project_BehaviorDescriptor() {
  }
  @Override
  public String getConceptFqName() {
    return "org.campagnelab.book.diagrams2.structure.Project";
  }
}
