package org.campagnelab.book.diagrams2.editor;

/*Generated by MPS */

import jetbrains.mps.nodeEditor.DefaultNodeEditor;
import jetbrains.mps.openapi.editor.cells.EditorCell;
import jetbrains.mps.openapi.editor.EditorContext;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.nodeEditor.cells.jetpad.BlockCell;
import jetbrains.mps.nodeEditor.cells.jetpad.PropertyMapperCell;
import jetbrains.mps.nodeEditor.cells.jetpad.ReadableModelProperty;
import jetbrains.jetpad.model.collections.list.ObservableList;
import jetbrains.jetpad.model.collections.list.ObservableArrayList;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SPropertyOperations;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import jetbrains.mps.util.Pair;
import org.jetbrains.mps.openapi.model.SNodeReference;
import jetbrains.mps.smodel.SNodePointer;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SLinkOperations;
import java.util.HashSet;
import jetbrains.jetpad.mapper.Mapper;
import jetbrains.jetpad.projectional.diagram.view.DiagramNodeView;
import jetbrains.jetpad.mapper.Synchronizers;
import jetbrains.jetpad.mapper.MapperFactory;
import jetbrains.jetpad.projectional.view.View;
import jetbrains.mps.nodeEditor.cells.jetpad.PortCell;
import jetbrains.mps.lang.editor.figures.library.NamedBoxFigure;
import jetbrains.mps.lang.editor.diagram.runtime.jetpad.views.MovableContentView;
import jetbrains.jetpad.model.property.WritableProperty;
import jetbrains.jetpad.geometry.Rectangle;
import jetbrains.mps.editor.runtime.selection.SelectionUtil;
import jetbrains.mps.internal.collections.runtime.Sequence;
import jetbrains.mps.nodeEditor.cells.jetpad.JetpadUtils;
import jetbrains.mps.nodeEditor.cells.jetpad.DiagramCell;
import jetbrains.mps.lang.editor.diagram.runtime.jetpad.views.NodeDecoratorView;
import jetbrains.jetpad.model.property.Properties;
import jetbrains.jetpad.geometry.Vector;
import jetbrains.mps.lang.editor.diagram.runtime.jetpad.views.ResizableContentView;
import jetbrains.mps.lang.editor.diagram.runtime.jetpad.views.PortDecoratorView;

public class Node_Editor extends DefaultNodeEditor {
  public EditorCell createEditorCell(EditorContext editorContext, SNode node) {
    return this.createDiagramNode_aofvvp_a(editorContext, node);
  }
  private EditorCell createDiagramNode_aofvvp_a(final EditorContext editorContext, final SNode node) {
    BlockCell editorCell = new Node_Editor.BlockCellImpl_aofvvp_a(editorContext, node);
    editorCell.setCellId("DiagramNode_aofvvp_a");
    editorCell.setBig(true);
    DefaultDiagramElementActionMap_0.setCellActions(editorCell, node, editorContext);
    return editorCell;
  }
  private class BlockCellImpl_aofvvp_a extends BlockCell {
    private final PropertyMapperCell<Integer> myPropertyCell_aofvvp_a0a;
    private final PropertyMapperCell<Integer> myPropertyCell_aofvvp_a1a;
    private final PropertyMapperCell<String> myPropertyCell_aofvvp_a2a;
    private final ReadableModelProperty<Boolean> myProperty_aofvvp_a3a;
    private final ObservableList<SNode> myInputPorts = new ObservableArrayList<SNode>();
    private final ObservableList<SNode> myOutputPorts = new ObservableArrayList<SNode>();
    private BlockCellImpl_aofvvp_a(EditorContext editorContext, final SNode node) {
      super(editorContext, node);
      myPropertyCell_aofvvp_a0a = new PropertyMapperCell<Integer>(editorContext, node) {
        protected Integer getModelPropertyValueImpl() {
          return SPropertyOperations.getInteger(node, MetaAdapterFactory.getProperty(0x5d2fbb2378f249edL, 0x9b872de983abb5c2L, 0x591f31273db9f65cL, 0x591f31273db9f9e8L, "x"));
        }
        protected void setModelPropertyValueImpl(Integer value) {
          SPropertyOperations.set(node, MetaAdapterFactory.getProperty(0x5d2fbb2378f249edL, 0x9b872de983abb5c2L, 0x591f31273db9f65cL, 0x591f31273db9f9e8L, "x"), "" + (value));
        }
      };
      addEditorCell(myPropertyCell_aofvvp_a0a);
      myPropertyCell_aofvvp_a0a.getEditorComponent().getUpdater().getCurrentUpdateSession().registerCleanDependency(myPropertyCell_aofvvp_a0a, new Pair<SNodeReference, String>(new SNodePointer(node), "x"));
      myPropertyCell_aofvvp_a1a = new PropertyMapperCell<Integer>(editorContext, node) {
        protected Integer getModelPropertyValueImpl() {
          return SPropertyOperations.getInteger(node, MetaAdapterFactory.getProperty(0x5d2fbb2378f249edL, 0x9b872de983abb5c2L, 0x591f31273db9f65cL, 0x591f31273db9f9eaL, "y"));
        }
        protected void setModelPropertyValueImpl(Integer value) {
          SPropertyOperations.set(node, MetaAdapterFactory.getProperty(0x5d2fbb2378f249edL, 0x9b872de983abb5c2L, 0x591f31273db9f65cL, 0x591f31273db9f9eaL, "y"), "" + (value));
        }
      };
      addEditorCell(myPropertyCell_aofvvp_a1a);
      myPropertyCell_aofvvp_a1a.getEditorComponent().getUpdater().getCurrentUpdateSession().registerCleanDependency(myPropertyCell_aofvvp_a1a, new Pair<SNodeReference, String>(new SNodePointer(node), "y"));
      myPropertyCell_aofvvp_a2a = new PropertyMapperCell<String>(editorContext, node) {
        protected String getModelPropertyValueImpl() {
          return SPropertyOperations.getString(node, MetaAdapterFactory.getProperty(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L, 0x110396ec041L, "name"));
        }
        protected void setModelPropertyValueImpl(String value) {
          SPropertyOperations.set(node, MetaAdapterFactory.getProperty(0xceab519525ea4f22L, 0x9b92103b95ca8c0cL, 0x110396eaaa4L, 0x110396ec041L, "name"), value);
        }
      };
      addEditorCell(myPropertyCell_aofvvp_a2a);
      myPropertyCell_aofvvp_a2a.getEditorComponent().getUpdater().getCurrentUpdateSession().registerCleanDependency(myPropertyCell_aofvvp_a2a, new Pair<SNodeReference, String>(new SNodePointer(node), "name"));
      myProperty_aofvvp_a3a = new ReadableModelProperty<Boolean>() {
        protected Boolean getModelPropertyValue() {
          return true;
        }
      };
      addModelProperty(myProperty_aofvvp_a3a);
      synchronize();
    }
    public void synchronize() {
      super.synchronizeViewWithModel();
      myPropertyCell_aofvvp_a0a.synchronize();
      myPropertyCell_aofvvp_a1a.synchronize();
      myPropertyCell_aofvvp_a2a.synchronize();
      syncPortNodes(SLinkOperations.getChildren(getSNode(), MetaAdapterFactory.getContainmentLink(0x5d2fbb2378f249edL, 0x9b872de983abb5c2L, 0x591f31273db9f65cL, 0x591f31273db9fe54L, "inputs")), myInputPorts.listIterator(), new HashSet<SNode>(myInputPorts));
      syncPortNodes(SLinkOperations.getChildren(getSNode(), MetaAdapterFactory.getContainmentLink(0x5d2fbb2378f249edL, 0x9b872de983abb5c2L, 0x591f31273db9f65cL, 0x591f31273db9fe5eL, "outputs")), myOutputPorts.listIterator(), new HashSet<SNode>(myOutputPorts));
    }
    public Mapper<SNode, DiagramNodeView> createMapper() {
      return new Mapper<SNode, DiagramNodeView>(getSNode(), createDiagramNodeView()) {
        @Override
        protected void registerSynchronizers(Mapper.SynchronizersConfiguration configuration) {
          super.registerSynchronizers(configuration);
          configuration.add(Synchronizers.forObservableRole(this, myInputPorts, getTarget().inputs.children(), new MapperFactory<SNode, View>() {
            public Mapper<? extends SNode, ? extends View> createMapper(SNode portNode) {
              return ((PortCell) getDirectChildCell(portNode)).createMapper();
            }
          }));
          configuration.add(Synchronizers.forObservableRole(this, myOutputPorts, getTarget().outputs.children(), new MapperFactory<SNode, View>() {
            public Mapper<? extends SNode, ? extends View> createMapper(SNode portNode) {
              return ((PortCell) getDirectChildCell(portNode)).createMapper();
            }
          }));
          final DiagramNodeView diagramNodeView = getTarget();
          configuration.add(Synchronizers.forConstantRole(this, getContentViewMapperSource(), getTarget().contentView.children(), new MapperFactory<String, NamedBoxFigure>() {
            public Mapper<? extends String, ? extends NamedBoxFigure> createMapper(String block) {
              return new Mapper<String, NamedBoxFigure>(block, new NamedBoxFigure()) {
                @Override
                protected void registerSynchronizers(Mapper.SynchronizersConfiguration configuration) {
                  super.registerSynchronizers(configuration);
                  configuration.add(Synchronizers.forProperty(getTarget().prop(MovableContentView.POSITION_X), new Runnable() {
                    public void run() {
                      updatePositionsFromModel(getTarget(), diagramNodeView);
                    }
                  }));
                  configuration.add(Synchronizers.forProperty(getTarget().prop(MovableContentView.POSITION_Y), new Runnable() {
                    public void run() {
                      updatePositionsFromModel(getTarget(), diagramNodeView);
                    }
                  }));
                  configuration.add(Synchronizers.forProperty(getTarget().bounds(), new WritableProperty<Rectangle>() {
                    public void set(Rectangle bounds) {
                      getTarget().prop(MovableContentView.POSITION_X).set(bounds.origin.x);
                      getTarget().prop(MovableContentView.POSITION_Y).set(bounds.origin.y);
                    }
                  }));
                  myPropertyCell_aofvvp_a0a.registerSynchronizers(configuration, getTarget().prop(MovableContentView.POSITION_X));
                  myPropertyCell_aofvvp_a1a.registerSynchronizers(configuration, getTarget().prop(MovableContentView.POSITION_Y));
                  myPropertyCell_aofvvp_a2a.registerSynchronizers(configuration, getTarget().nameText());
                  configuration.add(Synchronizers.forProperty(myProperty_aofvvp_a3a, getTarget().editable));
                }
              };
            }
          }));
          final View targetView = this.getTarget();
          configuration.add(Synchronizers.forProperty(targetView.focused(), new WritableProperty<Boolean>() {
            public void set(Boolean isFocused) {
              if (isFocused && !(isSelected())) {
                SelectionUtil.selectCell(getContext(), getSNode(), getCellId());
              }
            }
          }));
          configuration.add(Synchronizers.forProperty(mySelectedItem, new WritableProperty<Boolean>() {
            public void set(Boolean isSelected) {
              if (isSelected) {
                for (View view : Sequence.fromIterable(JetpadUtils.getAllChildren(targetView))) {
                  if (view.focused().get()) {
                    return;
                  }
                }
                targetView.container().focusedView().set(targetView);
              } else if (!(isSelected) && targetView.focused().get()) {
                targetView.container().focusedView().set(null);
              }
            }
          }));
          configuration.add(Synchronizers.forProperty(targetView.bounds(), new WritableProperty<Rectangle>() {
            public void set(Rectangle rect) {
              DiagramCell diagramCell = getDiagramCell();
              if (diagramCell == null) {
                return;
              }
              setX(rect.origin.x + diagramCell.getX());
              setY(rect.origin.y + diagramCell.getY());
              setWidth(rect.dimension.x);
              setHeight(rect.dimension.y);
            }
          }));
        }
      };
    }
    public Mapper<SNode, NodeDecoratorView> createDecorationMapper() {
      return new Mapper<SNode, NodeDecoratorView>(getSNode(), new NodeDecoratorView()) {
        @Override
        protected void registerSynchronizers(Mapper.SynchronizersConfiguration configuration) {
          super.registerSynchronizers(configuration);
          DiagramCell diagramCell = getDiagramCell();
          if (diagramCell == null) {
            return;
          }
          final Mapper<SNode, DiagramNodeView> blockMapper = getBlockMapper();
          if (blockMapper == null) {
            return;
          }
          configuration.add(Synchronizers.forProperty(myErrorItem, getTarget().hasError));
          configuration.add(Synchronizers.forProperty(blockMapper.getTarget().focused(), getTarget().isSelected));
          final NamedBoxFigure contentView = (NamedBoxFigure) getContentView();
          configuration.add(Synchronizers.forProperty(contentView.bounds(), getTarget().bounds));
          configuration.add(Synchronizers.forProperty(Properties.constant(Boolean.TRUE), getTarget().resizable));
          configuration.add(Synchronizers.forProperty(getTarget().boundsDelta, new WritableProperty<Rectangle>() {
            public void set(Rectangle delta) {
              if (delta == null) {
                return;
              }
              Vector positionDelta = delta.origin;
              Vector sizeDelta = delta.dimension;
              blockMapper.getTarget().move(positionDelta);
              contentView.prop(ResizableContentView.PREFERRED_SIZE).set(contentView.prop(ResizableContentView.PREFERRED_SIZE).get().add(sizeDelta));
            }
          }));
          configuration.add(Synchronizers.forObservableRole(this, myInputPorts, getTarget().inputPortDecotatorView.children(), new MapperFactory<SNode, PortDecoratorView>() {
            public Mapper<? extends SNode, ? extends PortDecoratorView> createMapper(SNode portNode) {
              return ((PortCell) getDirectChildCell(portNode)).createDecorationMapper();
            }
          }));
          configuration.add(Synchronizers.forObservableRole(this, myOutputPorts, getTarget().outputPortDecotatorView.children(), new MapperFactory<SNode, PortDecoratorView>() {
            public Mapper<? extends SNode, ? extends PortDecoratorView> createMapper(SNode portNode) {
              return ((PortCell) getDirectChildCell(portNode)).createDecorationMapper();
            }
          }));

        }
      };
    }
  }
}
