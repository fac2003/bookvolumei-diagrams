<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:fb444c90-041c-47e2-9bae-84f824ee69b7(org.campagnelab.book.diagrams3.behavior)">
  <persistence version="9" />
  <languages>
    <use id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="e2lb" ref="f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.lang(JDK/java.lang@java_stub)" implicit="true" />
    <import index="tonp" ref="r:70fb7011-8bd4-420a-9cdc-5f78c3673d76(org.campagnelab.book.diagrams3.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="af65afd8-f0dd-4942-87d9-63a55f2a9db1" name="jetbrains.mps.lang.behavior">
      <concept id="1225194240794" name="jetbrains.mps.lang.behavior.structure.ConceptBehavior" flags="ng" index="13h7C7">
        <reference id="1225194240799" name="concept" index="13h7C2" />
        <child id="1225194240805" name="method" index="13h7CS" />
        <child id="1225194240801" name="constructor" index="13h7CW" />
      </concept>
      <concept id="1225194413805" name="jetbrains.mps.lang.behavior.structure.ConceptConstructorDeclaration" flags="in" index="13hLZK" />
      <concept id="1225194472830" name="jetbrains.mps.lang.behavior.structure.ConceptMethodDeclaration" flags="ng" index="13i0hz" />
      <concept id="1225194691553" name="jetbrains.mps.lang.behavior.structure.ThisNodeExpression" flags="nn" index="13iPFW" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1151688443754" name="jetbrains.mps.baseLanguage.collections.structure.ListType" flags="in" index="_YKpA">
        <child id="1151688676805" name="elementType" index="_ZDj9" />
      </concept>
      <concept id="1237721394592" name="jetbrains.mps.baseLanguage.collections.structure.AbstractContainerCreator" flags="nn" index="HWqM0">
        <child id="1237721435808" name="initValue" index="HW$Y0" />
        <child id="1237721435807" name="elementType" index="HW$YZ" />
      </concept>
      <concept id="1160600644654" name="jetbrains.mps.baseLanguage.collections.structure.ListCreatorWithInit" flags="nn" index="Tc6Ow" />
    </language>
  </registry>
  <node concept="13h7C7" id="4Q9edAs9Wj3">
    <ref role="13h7C2" to="tonp:4Q9edAs80dD" resolve="NodeRendering" />
    <node concept="13i0hz" id="4Q9edAsa0fV" role="13h7CS">
      <property role="TrG5h" value="retrieveInputPorts" />
      <node concept="3Tm1VV" id="4Q9edAsa0fW" role="1B3o_S" />
      <node concept="_YKpA" id="4Q9edAsa0fX" role="3clF45">
        <node concept="3uibUv" id="4Q9edAsa0fY" role="_ZDj9">
          <ref role="3uigEE" to="e2lb:~Object" resolve="Object" />
        </node>
      </node>
      <node concept="3clFbS" id="4Q9edAsa0fZ" role="3clF47">
        <node concept="3cpWs6" id="4Q9edAsa0g0" role="3cqZAp">
          <node concept="2ShNRf" id="4Q9edAsa0g1" role="3cqZAk">
            <node concept="Tc6Ow" id="4Q9edAsa0g2" role="2ShVmc">
              <node concept="2OqwBi" id="4Q9edAscZil" role="HW$Y0">
                <node concept="13iPFW" id="4Q9edAscZeq" role="2Oq$k0" />
                <node concept="3TrEf2" id="4Q9edAscZDw" role="2OqNvi">
                  <ref role="3Tt5mk" to="tonp:4Q9edAscYqI" />
                </node>
              </node>
              <node concept="3uibUv" id="4Q9edAsa0g5" role="HW$YZ">
                <ref role="3uigEE" to="e2lb:~Object" resolve="Object" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13i0hz" id="4Q9edAs9WjT" role="13h7CS">
      <property role="TrG5h" value="retrieveOutputPorts" />
      <node concept="3Tm1VV" id="4Q9edAs9WjU" role="1B3o_S" />
      <node concept="_YKpA" id="4Q9edAs9Wmb" role="3clF45">
        <node concept="3uibUv" id="4Q9edAs9Wmh" role="_ZDj9">
          <ref role="3uigEE" to="e2lb:~Object" resolve="Object" />
        </node>
      </node>
      <node concept="3clFbS" id="4Q9edAs9WjW" role="3clF47">
        <node concept="3cpWs6" id="4Q9edAs9Wml" role="3cqZAp">
          <node concept="2ShNRf" id="4Q9edAs9WE5" role="3cqZAk">
            <node concept="Tc6Ow" id="4Q9edAs9WK1" role="2ShVmc">
              <node concept="2OqwBi" id="4Q9edAsd0yS" role="HW$Y0">
                <node concept="13iPFW" id="4Q9edAsd0uX" role="2Oq$k0" />
                <node concept="3TrEf2" id="4Q9edAsd0JG" role="2OqNvi">
                  <ref role="3Tt5mk" to="tonp:4Q9edAscYqM" />
                </node>
              </node>
              <node concept="3uibUv" id="4Q9edAs9XI7" role="HW$YZ">
                <ref role="3uigEE" to="e2lb:~Object" resolve="Object" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="13hLZK" id="4Q9edAs9Wj4" role="13h7CW">
      <node concept="3clFbS" id="4Q9edAs9Wj5" role="2VODD2" />
    </node>
  </node>
</model>

