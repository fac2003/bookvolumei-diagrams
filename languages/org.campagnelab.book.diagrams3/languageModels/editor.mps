<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:b17963fe-af3f-4337-a41e-b8dabe7cf5a7(org.campagnelab.book.diagrams3.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="-1" />
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="-1" />
    <use id="ed6d7656-532c-4bc2-81d1-af945aeb8280" name="jetbrains.mps.baseLanguage.blTypes" version="-1" />
    <use id="93bc01ac-08ca-4f11-9c7d-614d04055dfb" name="org.campagnelab.mps.editor2pdf" version="-1" />
    <use id="443f4c36-fcf5-4eb6-9500-8d06ed259e3e" name="jetbrains.mps.baseLanguage.classifiers" version="-1" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="-1" />
    <use id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts" version="-1" />
    <use id="9ded098b-ad6a-4657-bfd9-48636cfe8bc3" name="jetbrains.mps.lang.traceable" version="-1" />
    <use id="6106f611-7a74-42d1-80de-edc5c602bfd1" name="jetbrains.mps.lang.editor.diagram" version="-1" />
    <use id="d7722d50-4b93-4c3a-ae06-1903d05f95a7" name="jetbrains.mps.lang.editor.figures" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="8tro" ref="r:257a7f19-40a4-4037-a93b-ce1b638af281(jetbrains.mps.lang.editor.figures.library)" />
    <import index="8zlp" ref="r:fb444c90-041c-47e2-9bae-84f824ee69b7(org.campagnelab.book.diagrams3.behavior)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="e2lb" ref="f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.lang(JDK/java.lang@java_stub)" implicit="true" />
    <import index="tonp" ref="r:70fb7011-8bd4-420a-9cdc-5f78c3673d76(org.campagnelab.book.diagrams3.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="93bc01ac-08ca-4f11-9c7d-614d04055dfb" name="org.campagnelab.mps.editor2pdf">
      <concept id="893392931327129896" name="org.campagnelab.mps.editor2pdf.structure.DiagramOutputDirectory" flags="ng" index="KZc4b">
        <property id="893392931327129956" name="path" index="KZc57" />
      </concept>
      <concept id="8751972264247112684" name="org.campagnelab.mps.editor2pdf.structure.EditorAnnotation" flags="ng" index="3ZW7eb">
        <property id="5378718574870043633" name="outputFormat" index="2ripvU" />
        <reference id="893392931327136863" name="outputTo" index="KZaLW" />
      </concept>
    </language>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
    </language>
    <language id="6106f611-7a74-42d1-80de-edc5c602bfd1" name="jetbrains.mps.lang.editor.diagram">
      <concept id="1094405431463454433" name="jetbrains.mps.lang.editor.diagram.structure.CellModel_DiagramNode" flags="sg" stub="730538219795610279" index="9$NOg">
        <child id="1094405431463455193" name="figure" index="9$N8C" />
        <child id="1094405431463761842" name="parameters" index="9_WL3" />
        <child id="2084788800269090635" name="inputPort" index="zbHsl" />
        <child id="2084788800269090678" name="outputPort" index="zbHsC" />
      </concept>
      <concept id="1094405431463761863" name="jetbrains.mps.lang.editor.diagram.structure.FigureParameterMapping" flags="ng" index="9_WKQ">
        <child id="285670992218957021" name="argument" index="3YbGMt" />
      </concept>
      <concept id="6306886970791033847" name="jetbrains.mps.lang.editor.diagram.structure.CellModel_Diagram" flags="sg" stub="730538219795567478" index="2b3QIZ">
        <child id="8570854907290721333" name="elementsCreation" index="3cyXsl" />
        <child id="939897302409114961" name="connectorCreation" index="3Iu_Fc" />
        <child id="5355858557208539148" name="diagramElements" index="1VXmjR" />
      </concept>
      <concept id="1301388602725986966" name="jetbrains.mps.lang.editor.diagram.structure.AbstractDiagramCreation" flags="ng" index="mdwis">
        <reference id="1301388602726005553" name="concept" index="mdGOV" />
        <child id="1301388602726005547" name="query" index="mdGOx" />
      </concept>
      <concept id="6382742553261733065" name="jetbrains.mps.lang.editor.diagram.structure.CellModel_DiagramConnector" flags="sg" stub="730538219795610242" index="2FuRD1">
        <child id="1220375669566529919" name="input" index="2PTkhb" />
        <child id="1220375669566529925" name="output" index="2PTkiL" />
      </concept>
      <concept id="1220375669566347117" name="jetbrains.mps.lang.editor.diagram.structure.ConnectionEndBLQuery" flags="ng" index="2PTV9p">
        <child id="1220375669566421348" name="pointID" index="2PTDLg" />
        <child id="2915596886892604954" name="targetNode" index="3B0qBL" />
      </concept>
      <concept id="8570854907290423690" name="jetbrains.mps.lang.editor.diagram.structure.DiagramElementsCreation" flags="ng" index="3cx5EE">
        <child id="8570854907290527457" name="handler" index="3cxIR1" />
      </concept>
      <concept id="8570854907290527479" name="jetbrains.mps.lang.editor.diagram.structure.DiagramElementCreationHandler" flags="ig" index="3cxIRn" />
      <concept id="8570854907290717922" name="jetbrains.mps.lang.editor.diagram.structure.XFunctionParameter" flags="ng" index="3cyWn2" />
      <concept id="8570854907290717911" name="jetbrains.mps.lang.editor.diagram.structure.YFunctionParameter" flags="ng" index="3cyWnR" />
      <concept id="8570854907290717918" name="jetbrains.mps.lang.editor.diagram.structure.NodeFunctionParameter" flags="ng" index="3cyWnY" />
      <concept id="5422656561926747342" name="jetbrains.mps.lang.editor.diagram.structure.AttributedFigureReference" flags="ng" index="3FP96B">
        <reference id="5422656561931890753" name="figureAttribute" index="3FDhkC" />
      </concept>
      <concept id="939897302409170270" name="jetbrains.mps.lang.editor.diagram.structure.ToNodeFunctionParameter" flags="ng" index="3Iumb3" />
      <concept id="939897302409170265" name="jetbrains.mps.lang.editor.diagram.structure.FromNodeFunctionParameter" flags="ng" index="3Iumb4" />
      <concept id="939897302409170280" name="jetbrains.mps.lang.editor.diagram.structure.ToIdFunctionParameter" flags="ng" index="3IumbP" />
      <concept id="939897302409170275" name="jetbrains.mps.lang.editor.diagram.structure.FromIdFunctionParameter" flags="ng" index="3IumbY" />
      <concept id="939897302409084996" name="jetbrains.mps.lang.editor.diagram.structure.DiagramConnectorCreation" flags="ng" index="3IuyZp">
        <child id="939897302409084999" name="canCreate" index="3IuyZq" />
        <child id="939897302409114956" name="handler" index="3Iu_Fh" />
      </concept>
      <concept id="939897302409085052" name="jetbrains.mps.lang.editor.diagram.structure.DiagramConnectorCreationHandler" flags="ig" index="3IuyZx" />
      <concept id="939897302409110350" name="jetbrains.mps.lang.editor.diagram.structure.DiagramConnectorCanCreateHandler" flags="ig" index="3Iu$Nj" />
      <concept id="3229274890673749551" name="jetbrains.mps.lang.editor.diagram.structure.ThisEditorNodeExpression" flags="ng" index="1SoGT8" />
      <concept id="5355858557208817201" name="jetbrains.mps.lang.editor.diagram.structure.DiagramElementBLQuery" flags="ng" index="1VYjFa">
        <child id="5355858557208817241" name="query" index="1VYjEy" />
      </concept>
      <concept id="285670992217672837" name="jetbrains.mps.lang.editor.diagram.structure.PropertyArgument" flags="ng" index="3YcAj5">
        <reference id="285670992217689748" name="property" index="3Ycyrk" />
      </concept>
      <concept id="285670992213637367" name="jetbrains.mps.lang.editor.diagram.structure.BLQueryArgument" flags="ng" index="3Ys12R">
        <child id="285670992213637368" name="query" index="3Ys12S" />
      </concept>
    </language>
    <language id="760a0a8c-eabb-4521-8bfd-65db761a9ba3" name="jetbrains.mps.baseLanguage.logging">
      <concept id="1167227138527" name="jetbrains.mps.baseLanguage.logging.structure.LogStatement" flags="nn" index="34ab3g">
        <property id="1167245565795" name="severity" index="35gtTG" />
        <child id="1167227463056" name="logExpression" index="34bqiv" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI">
        <reference id="1140138128738" name="concept" index="1PxNhF" />
        <child id="1140138123956" name="leftExpression" index="1PxMeX" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="KZc4b" id="7oa9SMKrbkG">
    <property role="TrG5h" value="FIGURES" />
    <property role="KZc57" value="/Users/fac2003/Dropbox/MPS_Book/figures" />
  </node>
  <node concept="24kQdi" id="7oa9SMKrAcE">
    <ref role="1XX52x" to="tonp:7oa9SMKr9Pt" resolve="Project" />
    <node concept="2b3QIZ" id="7oa9SMKrAd2" role="2wV5jI">
      <node concept="3IuyZp" id="4Q9edAsawf2" role="3Iu_Fc">
        <property role="TrG5h" value="CreateConnector" />
        <ref role="mdGOV" to="tonp:4Q9edAs9o3i" resolve="EdgeRendering" />
        <node concept="3Iu$Nj" id="4Q9edAsawf3" role="3IuyZq">
          <node concept="3clFbS" id="4Q9edAsawf4" role="2VODD2">
            <node concept="3clFbF" id="4Q9edAsawUk" role="3cqZAp">
              <node concept="3clFbT" id="4Q9edAsawUj" role="3clFbG">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3IuyZx" id="4Q9edAsawf5" role="3Iu_Fh">
          <node concept="3clFbS" id="4Q9edAsawf6" role="2VODD2">
            <node concept="3clFbF" id="4Q9edAsbY1W" role="3cqZAp">
              <node concept="37vLTI" id="4Q9edAsbYNe" role="3clFbG">
                <node concept="1PxgMI" id="4Q9edAsbZ8q" role="37vLTx">
                  <ref role="1PxNhF" to="tonp:4Q9edAs80dD" resolve="NodeRendering" />
                  <node concept="3Iumb4" id="4Q9edAsbYNY" role="1PxMeX" />
                </node>
                <node concept="2OqwBi" id="4Q9edAsbY50" role="37vLTJ">
                  <node concept="3cyWnY" id="4Q9edAsbY1U" role="2Oq$k0" />
                  <node concept="3TrEf2" id="4Q9edAsbYA1" role="2OqNvi">
                    <ref role="3Tt5mk" to="tonp:4Q9edAsbVjj" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4Q9edAsbZeR" role="3cqZAp">
              <node concept="37vLTI" id="4Q9edAsbZeS" role="3clFbG">
                <node concept="1PxgMI" id="4Q9edAsbZeT" role="37vLTx">
                  <ref role="1PxNhF" to="tonp:4Q9edAs80dD" resolve="NodeRendering" />
                  <node concept="3Iumb3" id="4Q9edAsbZRo" role="1PxMeX" />
                </node>
                <node concept="2OqwBi" id="4Q9edAsbZeV" role="37vLTJ">
                  <node concept="3cyWnY" id="4Q9edAsbZeW" role="2Oq$k0" />
                  <node concept="3TrEf2" id="4Q9edAsbZOa" role="2OqNvi">
                    <ref role="3Tt5mk" to="tonp:4Q9edAsbVjl" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4Q9edAsaXUw" role="3cqZAp">
              <node concept="37vLTI" id="4Q9edAsaYDS" role="3clFbG">
                <node concept="2OqwBi" id="4Q9edAsaZPf" role="37vLTx">
                  <node concept="3IumbY" id="4Q9edAsaYKG" role="2Oq$k0" />
                  <node concept="liA8E" id="4Q9edAsaZUI" role="2OqNvi">
                    <ref role="37wK5l" to="e2lb:~Object.toString():java.lang.String" resolve="toString" />
                  </node>
                </node>
                <node concept="2OqwBi" id="4Q9edAsaXWe" role="37vLTJ">
                  <node concept="3cyWnY" id="4Q9edAsaXUu" role="2Oq$k0" />
                  <node concept="3TrcHB" id="4Q9edAsaYov" role="2OqNvi">
                    <ref role="3TsBF5" to="tonp:4Q9edAsaVhB" resolve="sourceId" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4Q9edAsaYRt" role="3cqZAp">
              <node concept="37vLTI" id="4Q9edAsaYRu" role="3clFbG">
                <node concept="2OqwBi" id="4Q9edAsaZC0" role="37vLTx">
                  <node concept="3IumbP" id="4Q9edAsaZwR" role="2Oq$k0" />
                  <node concept="liA8E" id="4Q9edAsaZHv" role="2OqNvi">
                    <ref role="37wK5l" to="e2lb:~Object.toString():java.lang.String" resolve="toString" />
                  </node>
                </node>
                <node concept="2OqwBi" id="4Q9edAsaYRw" role="37vLTJ">
                  <node concept="3cyWnY" id="4Q9edAsaYRx" role="2Oq$k0" />
                  <node concept="3TrcHB" id="4Q9edAsaZq0" role="2OqNvi">
                    <ref role="3TsBF5" to="tonp:4Q9edAsaVhD" resolve="destinationId" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="34ab3g" id="4Q9edAsbBWT" role="3cqZAp">
              <property role="35gtTG" value="error" />
              <node concept="3cpWs3" id="4Q9edAsbCf0" role="34bqiv">
                <node concept="2OqwBi" id="4Q9edAsbCjN" role="3uHU7w">
                  <node concept="3cyWnY" id="4Q9edAsbCfz" role="2Oq$k0" />
                  <node concept="3TrEf2" id="4Q9edAscqjU" role="2OqNvi">
                    <ref role="3Tt5mk" to="tonp:4Q9edAsbVjj" />
                  </node>
                </node>
                <node concept="Xl_RD" id="4Q9edAsbBWV" role="3uHU7B">
                  <property role="Xl_RC" value="node.source=" />
                </node>
              </node>
            </node>
            <node concept="34ab3g" id="4Q9edAsc0M4" role="3cqZAp">
              <property role="35gtTG" value="error" />
              <node concept="3cpWs3" id="4Q9edAsc0M5" role="34bqiv">
                <node concept="2OqwBi" id="4Q9edAsc0M6" role="3uHU7w">
                  <node concept="3cyWnY" id="4Q9edAsc0M7" role="2Oq$k0" />
                  <node concept="3TrEf2" id="4Q9edAsc1La" role="2OqNvi">
                    <ref role="3Tt5mk" to="tonp:4Q9edAsbVjl" />
                  </node>
                </node>
                <node concept="Xl_RD" id="4Q9edAsc0M9" role="3uHU7B">
                  <property role="Xl_RC" value="node.destination" />
                </node>
              </node>
            </node>
            <node concept="3clFbH" id="4Q9edAsc0nd" role="3cqZAp" />
          </node>
        </node>
        <node concept="2OqwBi" id="4Q9edAsawAK" role="mdGOx">
          <node concept="1SoGT8" id="4Q9edAsaw_C" role="2Oq$k0" />
          <node concept="3Tsc0h" id="4Q9edAsawJ9" role="2OqNvi">
            <ref role="3TtcxE" to="tonp:7oa9SMKr9Pw" />
          </node>
        </node>
      </node>
      <node concept="3cx5EE" id="4Q9edAs8DNO" role="3cyXsl">
        <property role="TrG5h" value="CreateNodeRendering" />
        <ref role="mdGOV" to="tonp:4Q9edAs80dD" resolve="NodeRendering" />
        <node concept="3cxIRn" id="4Q9edAs8DNP" role="3cxIR1">
          <node concept="3clFbS" id="4Q9edAs8DNQ" role="2VODD2">
            <node concept="3clFbF" id="4Q9edAs8Fw8" role="3cqZAp">
              <node concept="37vLTI" id="4Q9edAs8Gm_" role="3clFbG">
                <node concept="3cyWn2" id="4Q9edAs8GmR" role="37vLTx" />
                <node concept="2OqwBi" id="4Q9edAs8Fy0" role="37vLTJ">
                  <node concept="3cyWnY" id="4Q9edAs8Fw7" role="2Oq$k0" />
                  <node concept="3TrcHB" id="4Q9edAs8FTQ" role="2OqNvi">
                    <ref role="3TsBF5" to="tonp:4Q9edAs80et" resolve="x" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4Q9edAs8Gzt" role="3cqZAp">
              <node concept="37vLTI" id="4Q9edAs8Hek" role="3clFbG">
                <node concept="3cyWnR" id="4Q9edAs8HeA" role="37vLTx" />
                <node concept="2OqwBi" id="4Q9edAs8G_o" role="37vLTJ">
                  <node concept="3cyWnY" id="4Q9edAs8Gzr" role="2Oq$k0" />
                  <node concept="3TrcHB" id="4Q9edAs8GLy" role="2OqNvi">
                    <ref role="3TsBF5" to="tonp:4Q9edAs80ex" resolve="y" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="4Q9edAs9e7f" role="3cqZAp">
              <node concept="37vLTI" id="4Q9edAs9eBM" role="3clFbG">
                <node concept="Xl_RD" id="4Q9edAs9eC4" role="37vLTx">
                  <property role="Xl_RC" value="NEW" />
                </node>
                <node concept="2OqwBi" id="4Q9edAs9e9d" role="37vLTJ">
                  <node concept="3cyWnY" id="4Q9edAs9e7d" role="2Oq$k0" />
                  <node concept="3TrcHB" id="4Q9edAs9elK" role="2OqNvi">
                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="4Q9edAs8Fm$" role="mdGOx">
          <node concept="1SoGT8" id="4Q9edAs8Fls" role="2Oq$k0" />
          <node concept="3Tsc0h" id="4Q9edAs8FuX" role="2OqNvi">
            <ref role="3TtcxE" to="tonp:7oa9SMKr9Pu" />
          </node>
        </node>
      </node>
      <node concept="1VYjFa" id="7oa9SMKrAdj" role="1VXmjR">
        <node concept="2OqwBi" id="7oa9SMKrAfD" role="1VYjEy">
          <node concept="1SoGT8" id="7oa9SMKrAd_" role="2Oq$k0" />
          <node concept="3Tsc0h" id="7oa9SMKrAoa" role="2OqNvi">
            <ref role="3TtcxE" to="tonp:7oa9SMKr9Pu" />
          </node>
        </node>
      </node>
      <node concept="1VYjFa" id="4Q9edAs6K0x" role="1VXmjR">
        <node concept="2OqwBi" id="4Q9edAs6K30" role="1VYjEy">
          <node concept="1SoGT8" id="4Q9edAs6K1r" role="2Oq$k0" />
          <node concept="3Tsc0h" id="4Q9edAs70UP" role="2OqNvi">
            <ref role="3TtcxE" to="tonp:7oa9SMKr9Pw" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="4Q9edAs8oxB">
    <ref role="1XX52x" to="tonp:4Q9edAs80dD" resolve="NodeRendering" />
    <node concept="9$NOg" id="4Q9edAs8oxR" role="2wV5jI">
      <node concept="9_WKQ" id="4Q9edAs8pDQ" role="9_WL3">
        <property role="TrG5h" value="POSITION_X" />
        <node concept="3YcAj5" id="4Q9edAs8pH$" role="3YbGMt">
          <ref role="3Ycyrk" to="tonp:4Q9edAs80et" resolve="x" />
        </node>
      </node>
      <node concept="9_WKQ" id="4Q9edAs8pLL" role="9_WL3">
        <property role="TrG5h" value="POSITION_Y" />
        <node concept="3YcAj5" id="4Q9edAs8pNi" role="3YbGMt">
          <ref role="3Ycyrk" to="tonp:4Q9edAs80ex" resolve="y" />
        </node>
      </node>
      <node concept="9_WKQ" id="4Q9edAs8pOs" role="9_WL3">
        <property role="TrG5h" value="nameText" />
        <node concept="3YcAj5" id="4Q9edAs8pQw" role="3YbGMt">
          <ref role="3Ycyrk" to="tpck:h0TrG11" resolve="name" />
        </node>
      </node>
      <node concept="9_WKQ" id="4Q9edAs8pSq" role="9_WL3">
        <property role="TrG5h" value="editable" />
        <node concept="3Ys12R" id="4Q9edAs8q63" role="3YbGMt">
          <node concept="3clFbT" id="4Q9edAs8q8p" role="3Ys12S">
            <property role="3clFbU" value="true" />
          </node>
        </node>
      </node>
      <node concept="9_WKQ" id="4Q9edAs8qak" role="9_WL3">
        <property role="TrG5h" value="figureHeight" />
        <node concept="3Ys12R" id="4Q9edAs8qdB" role="3YbGMt">
          <node concept="3cmrfG" id="4Q9edAs8qgg" role="3Ys12S">
            <property role="3cmrfH" value="50" />
          </node>
        </node>
      </node>
      <node concept="9_WKQ" id="4Q9edAs8qiZ" role="9_WL3">
        <property role="TrG5h" value="figureWidth" />
        <node concept="3Ys12R" id="4Q9edAs8qwm" role="3YbGMt">
          <node concept="3cmrfG" id="4Q9edAs8qzx" role="3Ys12S">
            <property role="3cmrfH" value="100" />
          </node>
        </node>
      </node>
      <node concept="9_WKQ" id="4Q9edAs8qE0" role="9_WL3">
        <property role="TrG5h" value="lineWidth" />
        <node concept="3Ys12R" id="4Q9edAs8qIz" role="3YbGMt">
          <node concept="3cmrfG" id="4Q9edAs8qMg" role="3Ys12S">
            <property role="3cmrfH" value="3" />
          </node>
        </node>
      </node>
      <node concept="3FP96B" id="4Q9edAs8oSs" role="9$N8C">
        <ref role="3FDhkC" to="8tro:HEilRNywb_" />
      </node>
      <node concept="3Ys12R" id="4Q9edAs9VNO" role="zbHsl">
        <node concept="2OqwBi" id="4Q9edAs9VVt" role="3Ys12S">
          <node concept="1SoGT8" id="4Q9edAs9VSa" role="2Oq$k0" />
          <node concept="2qgKlT" id="4Q9edAsa1gT" role="2OqNvi">
            <ref role="37wK5l" to="8zlp:4Q9edAsa0fV" resolve="retrieveInputPorts" />
          </node>
        </node>
      </node>
      <node concept="3Ys12R" id="4Q9edAsa1_B" role="zbHsC">
        <node concept="2OqwBi" id="4Q9edAsa1QE" role="3Ys12S">
          <node concept="1SoGT8" id="4Q9edAsa1Kr" role="2Oq$k0" />
          <node concept="2qgKlT" id="4Q9edAsa2k8" role="2OqNvi">
            <ref role="37wK5l" to="8zlp:4Q9edAs9WjT" resolve="retrieveOutputPorts" />
          </node>
        </node>
      </node>
    </node>
    <node concept="3ZW7eb" id="4Q9edAs8pDm" role="lGtFl">
      <property role="2ripvU" value="1" />
      <property role="TrG5h" value="DiagramsNodeRenderingEditorWithFigImplAndParams" />
      <ref role="KZaLW" node="7oa9SMKrbkG" resolve="FIGURES" />
    </node>
  </node>
  <node concept="24kQdi" id="4Q9edAs9UMX">
    <ref role="1XX52x" to="tonp:4Q9edAs9o3i" resolve="EdgeRendering" />
    <node concept="2FuRD1" id="4Q9edAs9UMZ" role="2wV5jI">
      <node concept="2PTV9p" id="4Q9edAs9UO6" role="2PTkhb">
        <node concept="2OqwBi" id="4Q9edAs9UQ4" role="3B0qBL">
          <node concept="1SoGT8" id="4Q9edAs9UOo" role="2Oq$k0" />
          <node concept="3TrEf2" id="4Q9edAsc$NC" role="2OqNvi">
            <ref role="3Tt5mk" to="tonp:4Q9edAsbVjj" />
          </node>
        </node>
        <node concept="2OqwBi" id="4Q9edAsdI2A" role="2PTDLg">
          <node concept="2OqwBi" id="4Q9edAsdHxl" role="2Oq$k0">
            <node concept="1SoGT8" id="4Q9edAsdHvB" role="2Oq$k0" />
            <node concept="3TrEf2" id="4Q9edAsdHQN" role="2OqNvi">
              <ref role="3Tt5mk" to="tonp:4Q9edAsbVjj" />
            </node>
          </node>
          <node concept="3TrEf2" id="4Q9edAsdIf5" role="2OqNvi">
            <ref role="3Tt5mk" to="tonp:4Q9edAscYqM" />
          </node>
        </node>
      </node>
      <node concept="2PTV9p" id="4Q9edAs9VkR" role="2PTkiL">
        <node concept="2OqwBi" id="4Q9edAs9VmP" role="3B0qBL">
          <node concept="1SoGT8" id="4Q9edAs9Vl9" role="2Oq$k0" />
          <node concept="3TrEf2" id="4Q9edAsc_9S" role="2OqNvi">
            <ref role="3Tt5mk" to="tonp:4Q9edAsbVjl" />
          </node>
        </node>
        <node concept="2OqwBi" id="4Q9edAsdf6s" role="2PTDLg">
          <node concept="2OqwBi" id="4Q9edAsde54" role="2Oq$k0">
            <node concept="1SoGT8" id="4Q9edAsde3m" role="2Oq$k0" />
            <node concept="3TrEf2" id="4Q9edAsdeUD" role="2OqNvi">
              <ref role="3Tt5mk" to="tonp:4Q9edAsbVjl" />
            </node>
          </node>
          <node concept="3TrEf2" id="4Q9edAsdfk3" role="2OqNvi">
            <ref role="3Tt5mk" to="tonp:4Q9edAscYqI" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

