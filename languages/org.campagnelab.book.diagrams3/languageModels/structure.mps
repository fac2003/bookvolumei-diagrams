<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:70fb7011-8bd4-420a-9cdc-5f78c3673d76(org.campagnelab.book.diagrams3.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="-1" />
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="-1" />
    <use id="ed6d7656-532c-4bc2-81d1-af945aeb8280" name="jetbrains.mps.baseLanguage.blTypes" version="-1" />
    <use id="93bc01ac-08ca-4f11-9c7d-614d04055dfb" name="org.campagnelab.mps.editor2pdf" version="-1" />
    <use id="443f4c36-fcf5-4eb6-9500-8d06ed259e3e" name="jetbrains.mps.baseLanguage.classifiers" version="-1" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="-1" />
    <use id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts" version="-1" />
    <use id="9ded098b-ad6a-4657-bfd9-48636cfe8bc3" name="jetbrains.mps.lang.traceable" version="-1" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="93bc01ac-08ca-4f11-9c7d-614d04055dfb" name="org.campagnelab.mps.editor2pdf">
      <concept id="893392931327129896" name="org.campagnelab.mps.editor2pdf.structure.DiagramOutputDirectory" flags="ng" index="KZc4b">
        <property id="893392931327129956" name="path" index="KZc57" />
      </concept>
      <concept id="8751972264247112684" name="org.campagnelab.mps.editor2pdf.structure.EditorAnnotation" flags="ng" index="3ZW7eb">
        <property id="5378718574870043633" name="outputFormat" index="2ripvU" />
        <reference id="893392931327136863" name="outputTo" index="KZaLW" />
      </concept>
    </language>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <reference id="1071599698500" name="specializedLink" index="20ksaX" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="7oa9SMKqVR5">
    <property role="TrG5h" value="Node" />
    <property role="34LRSv" value="node" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="7oa9SMKr8my" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="3ZW7eb" id="7oa9SMKreyx" role="lGtFl">
      <property role="2ripvU" value="1" />
      <property role="TrG5h" value="DiagramsStructureNodeConcept" />
      <ref role="KZaLW" node="7oa9SMKrbkG" resolve="FIGURES" />
    </node>
  </node>
  <node concept="1TIwiD" id="7oa9SMKr8m$">
    <property role="TrG5h" value="Edge" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="7oa9SMKr9Pj" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="source" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="7oa9SMKqVR5" resolve="Node" />
    </node>
    <node concept="1TJgyj" id="7oa9SMKr9Pl" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="destination" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="7oa9SMKqVR5" resolve="Node" />
    </node>
    <node concept="3ZW7eb" id="7oa9SMKreyz" role="lGtFl">
      <property role="2ripvU" value="1" />
      <property role="TrG5h" value="DiagramsStructureEdgeConcept" />
      <ref role="KZaLW" node="7oa9SMKrbkG" resolve="FIGURES" />
    </node>
  </node>
  <node concept="1TIwiD" id="7oa9SMKr9Pt">
    <property role="TrG5h" value="Project" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="7oa9SMKr9Pu" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="nodes" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="7oa9SMKqVR5" resolve="Node" />
    </node>
    <node concept="1TJgyj" id="7oa9SMKr9Pw" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="edges" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="7oa9SMKr8m$" resolve="Edge" />
    </node>
    <node concept="3ZW7eb" id="7oa9SMKrbkE" role="lGtFl">
      <property role="2ripvU" value="1" />
      <property role="TrG5h" value="DiagramsProjectStructure" />
      <ref role="KZaLW" node="7oa9SMKrbkG" resolve="FIGURES" />
    </node>
  </node>
  <node concept="KZc4b" id="7oa9SMKrbkG">
    <property role="TrG5h" value="FIGURES" />
    <property role="KZc57" value="/Users/fac2003/Dropbox/MPS_Book/figures" />
  </node>
  <node concept="1TIwiD" id="4Q9edAs80dD">
    <property role="TrG5h" value="NodeRendering" />
    <ref role="1TJDcQ" node="7oa9SMKqVR5" resolve="Node" />
    <node concept="1TJgyj" id="4Q9edAscYqI" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="input" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="4Q9edAscYqK" resolve="InputPort" />
    </node>
    <node concept="1TJgyj" id="4Q9edAscYqM" role="1TKVEi">
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="output" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="4Q9edAscYqL" resolve="OutputPort" />
    </node>
    <node concept="1TJgyi" id="4Q9edAs80et" role="1TKVEl">
      <property role="TrG5h" value="x" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="4Q9edAs80ex" role="1TKVEl">
      <property role="TrG5h" value="y" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="4Q9edAs9o3i">
    <property role="TrG5h" value="EdgeRendering" />
    <ref role="1TJDcQ" node="7oa9SMKr8m$" resolve="Edge" />
    <node concept="1TJgyj" id="4Q9edAsbVjj" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="source" />
      <ref role="20ksaX" node="7oa9SMKr9Pj" />
      <ref role="20lvS9" node="4Q9edAs80dD" resolve="NodeRendering" />
    </node>
    <node concept="1TJgyj" id="4Q9edAsbVjl" role="1TKVEi">
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="destination" />
      <ref role="20ksaX" node="7oa9SMKr9Pl" />
      <ref role="20lvS9" node="4Q9edAs80dD" resolve="NodeRendering" />
    </node>
    <node concept="1TJgyi" id="4Q9edAsaVhB" role="1TKVEl">
      <property role="TrG5h" value="sourceId" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="4Q9edAsaVhD" role="1TKVEl">
      <property role="TrG5h" value="destinationId" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
  </node>
  <node concept="1TIwiD" id="4Q9edAscYqK">
    <property role="TrG5h" value="InputPort" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
  <node concept="1TIwiD" id="4Q9edAscYqL">
    <property role="TrG5h" value="OutputPort" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
  </node>
</model>

