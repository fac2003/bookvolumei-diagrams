<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:bd33feca-7a27-42b8-a0f3-54bebe53ca32(diagram.test1.A)">
  <persistence version="9" />
  <languages>
    <use id="050560c9-658e-49c5-b8e7-9e4db4c7e97f" name="jetbrains.mps.lang.editor.diagram.testLanguage" version="-1" />
    <use id="d5c1e0d2-5883-48fd-b91e-eedaeb4e5156" name="org.campagnelab.book.diagrams" version="-1" />
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="-1" />
    <use id="d7722d50-4b93-4c3a-ae06-1903d05f95a7" name="jetbrains.mps.lang.editor.figures" version="-1" />
    <use id="6106f611-7a74-42d1-80de-edc5c602bfd1" name="jetbrains.mps.lang.editor.diagram" version="-1" />
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="-1" />
    <use id="ed6d7656-532c-4bc2-81d1-af945aeb8280" name="jetbrains.mps.baseLanguage.blTypes" version="-1" />
    <use id="93bc01ac-08ca-4f11-9c7d-614d04055dfb" name="org.campagnelab.mps.editor2pdf" version="-1" />
    <use id="443f4c36-fcf5-4eb6-9500-8d06ed259e3e" name="jetbrains.mps.baseLanguage.classifiers" version="-1" />
    <use id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel" version="-1" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="-1" />
    <use id="13744753-c81f-424a-9c1b-cf8943bf4e86" name="jetbrains.mps.lang.sharedConcepts" version="-1" />
    <use id="9ded098b-ad6a-4657-bfd9-48636cfe8bc3" name="jetbrains.mps.lang.traceable" version="-1" />
    <use id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures" version="-1" />
    <use id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections" version="-1" />
    <use id="96ee7a94-411d-4cf8-9b94-96cad7e52411" name="jetbrains.mps.baseLanguage.jdk7" version="-1" />
    <use id="81f0abb8-d71e-4d13-a0c1-d2291fbb28b7" name="jetbrains.mps.lang.editor.editorTest" version="-1" />
    <use id="5d2fbb23-78f2-49ed-9b87-2de983abb5c2" name="org.campagnelab.book.diagrams2" version="-1" />
    <use id="61a16643-5a41-4a8b-8c59-bd29b0222972" name="org.campagnelab.book.diagrams3" version="-1" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="1" />
  </languages>
  <imports>
    <import index="8tro" ref="r:257a7f19-40a4-4037-a93b-ce1b638af281(jetbrains.mps.lang.editor.figures.library)" />
    <import index="4to0" ref="f:java_stub#67b3c41d-58b3-4756-b971-30bf8a9d63e6#jetbrains.jetpad.projectional.view(jetbrains.jetpad/jetbrains.jetpad.projectional.view@java_stub)" />
    <import index="k7g3" ref="f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.util(JDK/java.util@java_stub)" />
    <import index="ctj7" ref="f:java_stub#67b3c41d-58b3-4756-b971-30bf8a9d63e6#jetbrains.jetpad.model.property(jetbrains.jetpad/jetbrains.jetpad.model.property@java_stub)" />
    <import index="ew17" ref="f:java_stub#67b3c41d-58b3-4756-b971-30bf8a9d63e6#jetbrains.jetpad.values(jetbrains.jetpad/jetbrains.jetpad.values@java_stub)" />
    <import index="2qq2" ref="f:java_stub#67b3c41d-58b3-4756-b971-30bf8a9d63e6#jetbrains.jetpad.mapper(jetbrains.jetpad/jetbrains.jetpad.mapper@java_stub)" />
    <import index="racr" ref="f:java_stub#67b3c41d-58b3-4756-b971-30bf8a9d63e6#jetbrains.jetpad.cell(jetbrains.jetpad/jetbrains.jetpad.cell@java_stub)" />
    <import index="2ivk" ref="f:java_stub#67b3c41d-58b3-4756-b971-30bf8a9d63e6#jetbrains.jetpad.cell.text(jetbrains.jetpad/jetbrains.jetpad.cell.text@java_stub)" />
    <import index="qkka" ref="f:java_stub#67b3c41d-58b3-4756-b971-30bf8a9d63e6#jetbrains.jetpad.base(jetbrains.jetpad/jetbrains.jetpad.base@java_stub)" />
    <import index="e2lb" ref="f:java_stub#6354ebe7-c22a-4a0f-ac54-50b52ab9b065#java.lang(JDK/java.lang@java_stub)" implicit="true" />
  </imports>
  <registry>
    <language id="93bc01ac-08ca-4f11-9c7d-614d04055dfb" name="org.campagnelab.mps.editor2pdf">
      <concept id="893392931327129896" name="org.campagnelab.mps.editor2pdf.structure.DiagramOutputDirectory" flags="ng" index="KZc4b">
        <property id="893392931327129956" name="path" index="KZc57" />
      </concept>
      <concept id="8751972264247112684" name="org.campagnelab.mps.editor2pdf.structure.EditorAnnotation" flags="ng" index="3ZW7eb">
        <property id="5378718574870043633" name="outputFormat" index="2ripvU" />
        <reference id="893392931327136863" name="outputTo" index="KZaLW" />
      </concept>
    </language>
    <language id="61a16643-5a41-4a8b-8c59-bd29b0222972" name="org.campagnelab.book.diagrams3">
      <concept id="8505654331451409828" name="org.campagnelab.book.diagrams3.structure.Edge" flags="ng" index="36pE8C">
        <reference id="8505654331451415893" name="destination" index="36pFFp" />
        <reference id="8505654331451415891" name="source" index="36pFFv" />
      </concept>
      <concept id="8505654331451415901" name="org.campagnelab.book.diagrams3.structure.Project" flags="ng" index="36pFFh">
        <child id="8505654331451415902" name="nodes" index="36pFFi" />
        <child id="8505654331451415904" name="edges" index="36pFFG" />
      </concept>
      <concept id="5587059320008213353" name="org.campagnelab.book.diagrams3.structure.NodeRendering" flags="ng" index="3hEjIK">
        <property id="5587059320008213405" name="x" index="3hEjH4" />
        <property id="5587059320008213409" name="y" index="3hEjHS" />
        <child id="5587059320009516722" name="output" index="3hIHTF" />
        <child id="5587059320009516718" name="input" index="3hIHTR" />
      </concept>
      <concept id="5587059320008573138" name="org.campagnelab.book.diagrams3.structure.EdgeRendering" flags="ng" index="3hFbwb">
        <property id="5587059320008979561" name="destinationId" index="3hCCMK" />
        <property id="5587059320008979559" name="sourceId" index="3hCCMY" />
      </concept>
      <concept id="5587059320009516721" name="org.campagnelab.book.diagrams3.structure.OutputPort" flags="ng" index="3hIHTC" />
      <concept id="5587059320009516720" name="org.campagnelab.book.diagrams3.structure.InputPort" flags="ng" index="3hIHTD" />
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="2820489544401957797" name="jetbrains.mps.baseLanguage.structure.DefaultClassCreator" flags="nn" index="HV5vD">
        <reference id="2820489544401957798" name="classifier" index="HV5vE" />
      </concept>
      <concept id="1224848483129" name="jetbrains.mps.baseLanguage.structure.IBLDeprecatable" flags="ng" index="IEa8$">
        <property id="1224848525476" name="isDeprecated" index="IEkAT" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1197029447546" name="jetbrains.mps.baseLanguage.structure.FieldReferenceOperation" flags="nn" index="2OwXpG">
        <reference id="1197029500499" name="fieldDeclaration" index="2Oxat5" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070475587102" name="jetbrains.mps.baseLanguage.structure.SuperConstructorInvocation" flags="nn" index="XkiVB" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg">
        <property id="8606350594693632173" name="isTransient" index="eg7rD" />
        <property id="1240249534625" name="isVolatile" index="34CwA1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <child id="1095933932569" name="implementedInterface" index="EKbjA" />
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1109279763828" name="jetbrains.mps.baseLanguage.structure.TypeVariableDeclaration" flags="ng" index="16euLQ">
        <child id="1214996921760" name="bound" index="3ztrMU" />
      </concept>
      <concept id="1109279851642" name="jetbrains.mps.baseLanguage.structure.GenericDeclaration" flags="ng" index="16eOlS">
        <child id="1109279881614" name="typeVariableDeclaration" index="16eVyc" />
      </concept>
      <concept id="1109283449304" name="jetbrains.mps.baseLanguage.structure.TypeVariableReference" flags="in" index="16syzq">
        <reference id="1109283546497" name="typeVariableDeclaration" index="16sUi3" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1073063089578" name="jetbrains.mps.baseLanguage.structure.SuperMethodCall" flags="nn" index="3nyPlj" />
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk">
        <child id="1212687122400" name="typeParameter" index="1pMfVU" />
      </concept>
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <property id="521412098689998745" name="nonStatic" index="2bfB8j" />
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="1171903916106" name="jetbrains.mps.baseLanguage.structure.UpperBoundType" flags="in" index="3qUE_q">
        <child id="1171903916107" name="bound" index="3qUE_r" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="6329021646629104957" name="jetbrains.mps.baseLanguage.structure.TextCommentPart" flags="nn" index="3SKdUq">
        <property id="6329021646629104958" name="text" index="3SKdUp" />
      </concept>
      <concept id="6329021646629104954" name="jetbrains.mps.baseLanguage.structure.SingleLineComment" flags="nn" index="3SKdUt">
        <child id="6329021646629175155" name="commentPart" index="3SKWNk" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1146644641414" name="jetbrains.mps.baseLanguage.structure.ProtectedVisibility" flags="nn" index="3Tmbuc" />
      <concept id="1178893518978" name="jetbrains.mps.baseLanguage.structure.ThisConstructorInvocation" flags="nn" index="1VxSAg" />
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="5d2fbb23-78f2-49ed-9b87-2de983abb5c2" name="org.campagnelab.book.diagrams2">
      <concept id="6421905638262699612" name="org.campagnelab.book.diagrams2.structure.Node" flags="ng" index="1$DGtV">
        <property id="6421905638262700522" name="y" index="1$DGzd" />
        <property id="6421905638262700520" name="x" index="1$DGzf" />
        <child id="6421905638262701652" name="inputs" index="1$DGXN" />
        <child id="6421905638262701662" name="outputs" index="1$DGXT" />
      </concept>
      <concept id="6421905638262699668" name="org.campagnelab.book.diagrams2.structure.Edge" flags="ng" index="1$DGuN">
        <property id="2549630873159471140" name="sourcePortIndex" index="1$q0Jx" />
        <property id="2549630873159471162" name="destinationPortIndex" index="1$q0JZ" />
        <reference id="6421905638262699671" name="destination" index="1$DGuK" />
        <reference id="6421905638262699669" name="source" index="1$DGuM" />
      </concept>
      <concept id="6421905638262701659" name="org.campagnelab.book.diagrams2.structure.OutputPort" flags="ng" index="1$DGXW" />
      <concept id="6421905638262701656" name="org.campagnelab.book.diagrams2.structure.InputPort" flags="ng" index="1$DGXZ" />
      <concept id="2549630873157161993" name="org.campagnelab.book.diagrams2.structure.Project" flags="ng" index="1_xcZc">
        <child id="2549630873157163181" name="nodes" index="1_xcdC" />
        <child id="2549630873157163183" name="edges" index="1_xcdE" />
      </concept>
    </language>
    <language id="d5c1e0d2-5883-48fd-b91e-eedaeb4e5156" name="org.campagnelab.book.diagrams">
      <concept id="950699621305397019" name="org.campagnelab.book.diagrams.structure.Project" flags="ng" index="3dc3Xv">
        <child id="6421905638259567748" name="packages" index="1$_hQz" />
      </concept>
      <concept id="950699621305430973" name="org.campagnelab.book.diagrams.structure.Package" flags="ng" index="3dcUfT">
        <child id="6421905638259962147" name="contains" index="1$zK04" />
        <child id="6421905638261739298" name="outputs" index="1$H7S5" />
        <child id="6421905638261739293" name="inputs" index="1$H7SU" />
      </concept>
      <concept id="7271203974485764222" name="org.campagnelab.book.diagrams.structure.PackageElement" flags="ng" index="3wim$3">
        <property id="7271203974485074372" name="y" index="3wvY2T" />
        <property id="7271203974485074370" name="x" index="3wvY2Z" />
        <property id="7271203974485076876" name="height" index="3wvYFL" />
        <property id="7271203974485076872" name="width" index="3wvYFP" />
      </concept>
      <concept id="7271203974485869239" name="org.campagnelab.book.diagrams.structure.PackageInputPort" flags="ng" index="3wiKfa" />
      <concept id="6421905638261585825" name="org.campagnelab.book.diagrams.structure.PackageOutputPort" flags="ng" index="1$HWq6" />
    </language>
    <language id="d7722d50-4b93-4c3a-ae06-1903d05f95a7" name="jetbrains.mps.lang.editor.figures">
      <concept id="2178507174411801606" name="jetbrains.mps.lang.editor.figures.structure.ExternalViewFigureParameter" flags="ng" index="21jKyX">
        <reference id="2178507174411801649" name="fieldDeclaration" index="21jKya" />
      </concept>
      <concept id="2178507174411801538" name="jetbrains.mps.lang.editor.figures.structure.ExternalViewFigure" flags="ng" index="21jKXT">
        <reference id="2178507174411801591" name="classifier" index="21jKXc" />
        <child id="2178507174411801664" name="fields" index="21jKzV" />
      </concept>
      <concept id="2084788800270473590" name="jetbrains.mps.lang.editor.figures.structure.FigureParameterAttributeField" flags="ng" index="zeN4C" />
      <concept id="5422656561926747556" name="jetbrains.mps.lang.editor.figures.structure.FigureAttribute" flags="ng" index="3FP93d" />
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1133920641626" name="jetbrains.mps.lang.core.structure.BaseConcept" flags="ng" index="2VYdi">
        <child id="5169995583184591170" name="smodelAttribute" index="lGtFl" />
      </concept>
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="21jKXT" id="6jCwnQ4F5qs">
    <ref role="21jKXc" node="3ozeDDQ9We$" resolve="MyView" />
    <node concept="21jKyX" id="6nPrS2k1onq" role="21jKzV">
      <ref role="21jKya" node="1ZQTqiPofK4" resolve="width" />
    </node>
  </node>
  <node concept="312cEu" id="6jCwnQ4F5vn">
    <property role="TrG5h" value="NewFigureImplementation2" />
    <node concept="2tJIrI" id="3ozeDDQ9WdB" role="jymVt" />
    <node concept="2tJIrI" id="3ozeDDQ9Wdu" role="jymVt" />
    <node concept="2tJIrI" id="3ozeDDQ9Wdf" role="jymVt" />
    <node concept="2tJIrI" id="3ozeDDQ9Wd9" role="jymVt" />
    <node concept="3Tm1VV" id="6jCwnQ4F5vo" role="1B3o_S" />
    <node concept="3uibUv" id="6jCwnQ4F5vX" role="1zkMxy">
      <ref role="3uigEE" to="4to0:~View" resolve="View" />
    </node>
    <node concept="3FP93d" id="6jCwnQ4FaRQ" role="lGtFl">
      <node concept="3ZW7eb" id="3ozeDDQ9GaC" role="lGtFl">
        <property role="2ripvU" value="1" />
        <property role="TrG5h" value="NewFigureImplementation" />
        <ref role="KZaLW" node="6jCwnQ4$i6j" resolve="FIGURES" />
      </node>
    </node>
    <node concept="2tJIrI" id="3ozeDDQ9Wdm" role="jymVt" />
  </node>
  <node concept="KZc4b" id="6jCwnQ4$i6j">
    <property role="TrG5h" value="FIGURES" />
    <property role="KZc57" value="/Users/fac2003/Dropbox/MPS_Book/figures/" />
  </node>
  <node concept="312cEu" id="3ozeDDQ9We$">
    <property role="TrG5h" value="MyView" />
    <node concept="312cEg" id="1ZQTqiPofK4" role="jymVt">
      <property role="TrG5h" value="width" />
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <node concept="3Tm1VV" id="1ZQTqiPoeKF" role="1B3o_S" />
      <node concept="3uibUv" id="1ZQTqiPofIe" role="1tU5fm">
        <ref role="3uigEE" to="ctj7:~Property" resolve="Property" />
        <node concept="3uibUv" id="1TnYspSn_I0" role="11_B2D">
          <ref role="3uigEE" to="e2lb:~Integer" resolve="Integer" />
        </node>
      </node>
      <node concept="2ShNRf" id="1ZQTqiPogC3" role="33vP2m">
        <node concept="1pGfFk" id="1ZQTqiPogLX" role="2ShVmc">
          <ref role="37wK5l" to="ctj7:~ValueProperty.&lt;init&gt;(java.lang.Object)" resolve="ValueProperty" />
          <node concept="3uibUv" id="1TnYspSnAas" role="1pMfVU">
            <ref role="3uigEE" to="e2lb:~Integer" resolve="Integer" />
          </node>
          <node concept="3cmrfG" id="1TnYspSnACy" role="37wK5m">
            <property role="3cmrfH" value="100" />
          </node>
        </node>
      </node>
      <node concept="zeN4C" id="1ZQTqiPp5At" role="lGtFl" />
    </node>
    <node concept="2tJIrI" id="1TnYspSoRuw" role="jymVt" />
    <node concept="3clFbW" id="3ozeDDQbmAc" role="jymVt">
      <node concept="3cqZAl" id="3ozeDDQbmAd" role="3clF45" />
      <node concept="3clFbS" id="3ozeDDQbmAf" role="3clF47">
        <node concept="1VxSAg" id="1TnYspSoi2g" role="3cqZAp">
          <ref role="37wK5l" node="1TnYspSofAa" resolve="MyView" />
          <node concept="2ShNRf" id="1TnYspSoi2I" role="37wK5m">
            <node concept="HV5vD" id="1TnYspSooMz" role="2ShVmc">
              <ref role="HV5vE" node="1ZQTqiPlP6Z" resolve="MyView.MyViewFigureMapperFactory" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="3ozeDDQbmAg" role="1B3o_S" />
    </node>
    <node concept="3clFbW" id="1TnYspSofAa" role="jymVt">
      <node concept="3cqZAl" id="1TnYspSofAb" role="3clF45" />
      <node concept="3clFbS" id="1TnYspSofAd" role="3clF47" />
      <node concept="3Tm1VV" id="1TnYspSofAe" role="1B3o_S" />
      <node concept="37vLTG" id="1TnYspSohTq" role="3clF46">
        <property role="TrG5h" value="mapperFactory" />
        <node concept="3uibUv" id="1TnYspSohTp" role="1tU5fm">
          <ref role="3uigEE" node="1ZQTqiPlP6Z" resolve="MyView.MyViewFigureMapperFactory" />
        </node>
      </node>
    </node>
    <node concept="312cEu" id="1ZQTqiPlP6Z" role="jymVt">
      <property role="TrG5h" value="MyViewFigureMapperFactory" />
      <property role="2bfB8j" value="false" />
      <node concept="3clFb_" id="1ZQTqiPlP70" role="jymVt">
        <property role="IEkAT" value="false" />
        <property role="1EzhhJ" value="false" />
        <property role="TrG5h" value="createMapper" />
        <property role="DiZV1" value="false" />
        <node concept="3Tm1VV" id="1ZQTqiPlP71" role="1B3o_S" />
        <node concept="3uibUv" id="1ZQTqiPlP72" role="3clF45">
          <ref role="3uigEE" to="2qq2:~Mapper" resolve="Mapper" />
          <node concept="3qUE_q" id="1ZQTqiPlP73" role="11_B2D">
            <node concept="3uibUv" id="1TnYspSnS8$" role="3qUE_r">
              <ref role="3uigEE" node="3ozeDDQ9We$" resolve="MyView" />
            </node>
          </node>
          <node concept="3qUE_q" id="1ZQTqiPlP75" role="11_B2D">
            <node concept="3uibUv" id="1TnYspSnT10" role="3qUE_r">
              <ref role="3uigEE" node="3ozeDDQ9We$" resolve="MyView" />
            </node>
          </node>
        </node>
        <node concept="37vLTG" id="1ZQTqiPlP77" role="3clF46">
          <property role="TrG5h" value="figure" />
          <node concept="3uibUv" id="1TnYspSnEZz" role="1tU5fm">
            <ref role="3uigEE" node="3ozeDDQ9We$" resolve="MyView" />
          </node>
        </node>
        <node concept="3clFbS" id="1ZQTqiPlP79" role="3clF47">
          <node concept="3cpWs6" id="1ZQTqiPlP7a" role="3cqZAp">
            <node concept="2ShNRf" id="1ZQTqiPlP7b" role="3cqZAk">
              <node concept="1pGfFk" id="1ZQTqiPm1GP" role="2ShVmc">
                <ref role="37wK5l" node="1ZQTqiPlYad" resolve="MyView.MyViewFigureMapper" />
                <node concept="37vLTw" id="1ZQTqiPm1UL" role="37wK5m">
                  <ref role="3cqZAo" node="1ZQTqiPlP77" resolve="figure" />
                </node>
                <node concept="3uibUv" id="1TnYspSnTMi" role="1pMfVU">
                  <ref role="3uigEE" node="3ozeDDQ9We$" resolve="MyView" />
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="1ZQTqiPlP7e" role="1B3o_S" />
      <node concept="3uibUv" id="1ZQTqiPlP7f" role="EKbjA">
        <ref role="3uigEE" to="2qq2:~MapperFactory" resolve="MapperFactory" />
        <node concept="3uibUv" id="1TnYspSnRzG" role="11_B2D">
          <ref role="3uigEE" node="3ozeDDQ9We$" resolve="MyView" />
        </node>
        <node concept="3uibUv" id="1TnYspSnRQ8" role="11_B2D">
          <ref role="3uigEE" node="3ozeDDQ9We$" resolve="MyView" />
        </node>
      </node>
    </node>
    <node concept="312cEu" id="1ZQTqiPlQJx" role="jymVt">
      <property role="TrG5h" value="MyViewFigureMapper" />
      <property role="2bfB8j" value="false" />
      <node concept="3clFbW" id="1ZQTqiPlYad" role="jymVt">
        <node concept="37vLTG" id="1ZQTqiPlYb3" role="3clF46">
          <property role="TrG5h" value="figure" />
          <node concept="16syzq" id="1ZQTqiPlYbo" role="1tU5fm">
            <ref role="16sUi3" node="1ZQTqiPlVne" resolve="T" />
          </node>
        </node>
        <node concept="3cqZAl" id="1ZQTqiPlYaf" role="3clF45" />
        <node concept="3Tmbuc" id="1ZQTqiPlYag" role="1B3o_S" />
        <node concept="3clFbS" id="1ZQTqiPlYah" role="3clF47">
          <node concept="XkiVB" id="1ZQTqiPlYcc" role="3cqZAp">
            <ref role="37wK5l" to="2qq2:~Mapper.&lt;init&gt;(java.lang.Object,java.lang.Object)" resolve="Mapper" />
            <node concept="37vLTw" id="1ZQTqiPlYeg" role="37wK5m">
              <ref role="3cqZAo" node="1ZQTqiPlYb3" resolve="figure" />
            </node>
            <node concept="37vLTw" id="1TnYspSo4GH" role="37wK5m">
              <ref role="3cqZAo" node="1ZQTqiPlYb3" resolve="figure" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tmbuc" id="1ZQTqiPlQT6" role="1B3o_S" />
      <node concept="16euLQ" id="1ZQTqiPlVne" role="16eVyc">
        <property role="TrG5h" value="T" />
        <node concept="3uibUv" id="1TnYspSnUcY" role="3ztrMU">
          <ref role="3uigEE" node="3ozeDDQ9We$" resolve="MyView" />
        </node>
      </node>
      <node concept="3uibUv" id="1ZQTqiPlVMR" role="1zkMxy">
        <ref role="3uigEE" to="2qq2:~Mapper" resolve="Mapper" />
        <node concept="16syzq" id="1ZQTqiPlVNo" role="11_B2D">
          <ref role="16sUi3" node="1ZQTqiPlVne" resolve="T" />
        </node>
        <node concept="16syzq" id="1TnYspSo36V" role="11_B2D">
          <ref role="16sUi3" node="1ZQTqiPlVne" resolve="T" />
        </node>
      </node>
      <node concept="3clFb_" id="1TnYspSo4Hw" role="jymVt">
        <property role="1EzhhJ" value="false" />
        <property role="TrG5h" value="registerSynchronizers" />
        <property role="DiZV1" value="false" />
        <property role="IEkAT" value="false" />
        <node concept="3Tmbuc" id="1TnYspSo4Hx" role="1B3o_S" />
        <node concept="3cqZAl" id="1TnYspSo4Hz" role="3clF45" />
        <node concept="37vLTG" id="1TnYspSo4H$" role="3clF46">
          <property role="TrG5h" value="configuration" />
          <node concept="3uibUv" id="1TnYspSo4H_" role="1tU5fm">
            <ref role="3uigEE" to="2qq2:~Mapper$SynchronizersConfiguration" resolve="Mapper.SynchronizersConfiguration" />
          </node>
        </node>
        <node concept="3clFbS" id="1TnYspSo4HA" role="3clF47">
          <node concept="3clFbF" id="1TnYspSo4HE" role="3cqZAp">
            <node concept="3nyPlj" id="1TnYspSo4HD" role="3clFbG">
              <ref role="37wK5l" to="2qq2:~Mapper.registerSynchronizers(jetbrains.jetpad.mapper.Mapper$SynchronizersConfiguration):void" resolve="registerSynchronizers" />
              <node concept="37vLTw" id="1TnYspSo4HC" role="37wK5m">
                <ref role="3cqZAo" node="1TnYspSo4H$" resolve="configuration" />
              </node>
            </node>
          </node>
          <node concept="3SKdUt" id="1TnYspSoe_q" role="3cqZAp">
            <node concept="3SKdUq" id="1TnYspSoeAt" role="3SKWNk">
              <property role="3SKdUp" value="fetch property values from configuration and regerenate rendering elements to update" />
            </node>
          </node>
          <node concept="3SKdUt" id="1TnYspSoeCv" role="3cqZAp">
            <node concept="3SKdUq" id="1TnYspSoeCB" role="3SKWNk">
              <property role="3SKdUp" value="the diagram node display" />
            </node>
          </node>
          <node concept="3clFbH" id="1TnYspSoHot" role="3cqZAp" />
          <node concept="3clFbF" id="1ZQTqiP34h5" role="3cqZAp">
            <node concept="2OqwBi" id="1ZQTqiP34h6" role="3clFbG">
              <node concept="37vLTw" id="1ZQTqiP34h7" role="2Oq$k0">
                <ref role="3cqZAo" node="1TnYspSo4H$" resolve="configuration" />
              </node>
              <node concept="liA8E" id="1ZQTqiP34h8" role="2OqNvi">
                <ref role="37wK5l" to="2qq2:~Mapper$SynchronizersConfiguration.add(jetbrains.jetpad.mapper.Synchronizer):void" resolve="add" />
                <node concept="2YIFZM" id="1ZQTqiP34h9" role="37wK5m">
                  <ref role="37wK5l" to="2qq2:~Synchronizers.forProperty(jetbrains.jetpad.model.property.ReadableProperty,java.lang.Runnable):jetbrains.jetpad.mapper.Synchronizer" resolve="forProperty" />
                  <ref role="1Pybhc" to="2qq2:~Synchronizers" resolve="Synchronizers" />
                  <node concept="2OqwBi" id="1ZQTqiPjFC9" role="37wK5m">
                    <node concept="1rXfSq" id="1ZQTqiPjFWV" role="2Oq$k0">
                      <ref role="37wK5l" to="2qq2:~Mapper.getSource():java.lang.Object" resolve="getSource" />
                    </node>
                    <node concept="2OwXpG" id="1TnYspSoNfU" role="2OqNvi">
                      <ref role="2Oxat5" node="1ZQTqiPofK4" resolve="width" />
                    </node>
                  </node>
                  <node concept="1bVj0M" id="1ZQTqiP34hb" role="37wK5m">
                    <node concept="3clFbS" id="1ZQTqiP34hc" role="1bW5cS">
                      <node concept="3clFbF" id="1ZQTqiP34hd" role="3cqZAp">
                        <node concept="2OqwBi" id="1ZQTqiPjJa$" role="3clFbG">
                          <node concept="1rXfSq" id="1ZQTqiPjJvA" role="2Oq$k0">
                            <ref role="37wK5l" to="2qq2:~Mapper.getSource():java.lang.Object" resolve="getSource" />
                          </node>
                          <node concept="2OwXpG" id="1TnYspSoR1j" role="2OqNvi">
                            <ref role="2Oxat5" node="1ZQTqiPofK4" resolve="width" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="3clFbH" id="1TnYspSoHnn" role="3cqZAp" />
          <node concept="3clFbH" id="1TnYspSoHp$" role="3cqZAp" />
        </node>
        <node concept="2AHcQZ" id="1TnYspSo4HB" role="2AJF6D">
          <ref role="2AI5Lk" to="e2lb:~Override" resolve="Override" />
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="3ozeDDQ9WeD" role="1B3o_S" />
    <node concept="3uibUv" id="3ozeDDQ9WeE" role="1zkMxy">
      <ref role="3uigEE" to="4to0:~View" resolve="View" />
    </node>
    <node concept="3FP93d" id="3ozeDDQ9WeF" role="lGtFl">
      <node concept="3ZW7eb" id="1TnYspSoq$W" role="lGtFl">
        <property role="2ripvU" value="1" />
        <property role="TrG5h" value="MyViewMinimal" />
        <ref role="KZaLW" node="6jCwnQ4$i6j" resolve="FIGURES" />
      </node>
    </node>
    <node concept="2tJIrI" id="3ozeDDQ9WeH" role="jymVt" />
  </node>
  <node concept="3dc3Xv" id="5$vcisXG8kE">
    <property role="TrG5h" value="Diagram" />
    <node concept="3dcUfT" id="5$vcisXG8kF" role="1$_hQz">
      <property role="TrG5h" value="A" />
      <node concept="3dcUfT" id="5$vcisXG8kO" role="1$zK04">
        <property role="TrG5h" value="A.B" />
        <node concept="3dcUfT" id="5$vcisXG8kX" role="1$zK04">
          <property role="TrG5h" value="A.B.C" />
          <node concept="3wiKfa" id="5$vcisXG8kY" role="1$H7SU" />
          <node concept="1$HWq6" id="5$vcisXG8kZ" role="1$H7S5" />
        </node>
        <node concept="3wiKfa" id="5$vcisXG8kP" role="1$H7SU" />
        <node concept="1$HWq6" id="5$vcisXG8kQ" role="1$H7S5" />
      </node>
      <node concept="3wiKfa" id="5$vcisXG8kG" role="1$H7SU" />
      <node concept="1$HWq6" id="5$vcisXG8kH" role="1$H7S5" />
    </node>
    <node concept="3wim$3" id="5$vcisXHP3E" role="1$_hQz">
      <property role="3wvYFP" value="69" />
      <property role="3wvYFL" value="40" />
      <property role="3wvY2Z" value="253" />
      <property role="3wvY2T" value="78" />
      <property role="TrG5h" value="D" />
      <node concept="3wiKfa" id="5$vcisXHP3F" role="1$H7SU" />
      <node concept="1$HWq6" id="5$vcisXHP3G" role="1$H7S5" />
    </node>
    <node concept="3wim$3" id="5$vcisXHP45" role="1$_hQz">
      <property role="3wvYFP" value="100" />
      <property role="3wvYFL" value="40" />
      <property role="3wvY2Z" value="30" />
      <property role="3wvY2T" value="113" />
      <property role="TrG5h" value="C" />
      <node concept="3wiKfa" id="5$vcisXHP46" role="1$H7SU" />
      <node concept="1$HWq6" id="5$vcisXHP47" role="1$H7S5" />
    </node>
  </node>
  <node concept="1_xcZc" id="2dy6Wl6VA4t">
    <node concept="1$DGuN" id="2dy6Wl6Z24A" role="1_xcdE" />
    <node concept="1$DGuN" id="2dy6Wl6Z24G" role="1_xcdE" />
    <node concept="1$DGuN" id="2dy6Wl6Z24N" role="1_xcdE" />
    <node concept="1$DGuN" id="2dy6Wl6Z24V" role="1_xcdE" />
    <node concept="1$DGuN" id="2dy6Wl6Z24n" role="1_xcdE" />
    <node concept="1$DGuN" id="2dy6Wl6Z24q" role="1_xcdE" />
    <node concept="1$DGuN" id="2dy6Wl6Z24t" role="1_xcdE" />
    <node concept="1$DGuN" id="2dy6Wl6Z24x" role="1_xcdE" />
    <node concept="1$DGtV" id="2dy6Wl6VYyc" role="1_xcdC">
      <property role="1$DGzf" value="330" />
      <property role="1$DGzd" value="19" />
      <node concept="1$DGXZ" id="2dy6Wl6VYyd" role="1$DGXN" />
      <node concept="1$DGXW" id="2dy6Wl6VYye" role="1$DGXT" />
      <node concept="1$DGXZ" id="2dy6Wl73KyU" role="1$DGXN" />
      <node concept="1$DGXW" id="2dy6Wl74zdL" role="1$DGXT" />
      <node concept="1$DGXW" id="2dy6Wl74zeg" role="1$DGXT" />
    </node>
    <node concept="1$DGtV" id="2dy6Wl6VYyi" role="1_xcdC">
      <property role="1$DGzf" value="152" />
      <property role="1$DGzd" value="63" />
      <property role="TrG5h" value="Some Component" />
      <node concept="1$DGXZ" id="2dy6Wl6VYyj" role="1$DGXN" />
      <node concept="1$DGXW" id="2dy6Wl6VYyk" role="1$DGXT" />
    </node>
    <node concept="1$DGtV" id="2dy6Wl6Xbaq" role="1_xcdC">
      <property role="1$DGzf" value="428" />
      <property role="1$DGzd" value="139" />
      <node concept="1$DGXZ" id="2dy6Wl6Xbar" role="1$DGXN" />
      <node concept="1$DGXW" id="2dy6Wl6Xbas" role="1$DGXT" />
      <node concept="1$DGXZ" id="2dy6Wl72v8h" role="1$DGXN" />
      <node concept="1$DGXZ" id="2dy6Wl73Kyw" role="1$DGXN" />
      <node concept="1$DGXZ" id="2dy6Wl74zeh" role="1$DGXN" />
    </node>
    <node concept="1$DGuN" id="2dy6Wl70O11" role="1_xcdE" />
    <node concept="1$DGuN" id="2dy6Wl71g$T" role="1_xcdE">
      <ref role="1$DGuM" node="2dy6Wl6VYyi" resolve="Some Component" />
      <ref role="1$DGuK" node="2dy6Wl6VYyc" />
    </node>
    <node concept="1$DGuN" id="2dy6Wl71g_4" role="1_xcdE">
      <ref role="1$DGuM" node="2dy6Wl6VYyi" resolve="Some Component" />
      <ref role="1$DGuK" node="2dy6Wl6Xbaq" />
    </node>
    <node concept="1$DGuN" id="2dy6Wl71g_g" role="1_xcdE">
      <ref role="1$DGuM" node="2dy6Wl6VYyc" />
      <ref role="1$DGuK" node="2dy6Wl6Xbaq" />
    </node>
    <node concept="1$DGuN" id="2dy6Wl71g_t" role="1_xcdE">
      <ref role="1$DGuM" node="2dy6Wl6VYyc" />
      <ref role="1$DGuK" node="2dy6Wl6VYyi" resolve="Some Component" />
    </node>
    <node concept="1$DGtV" id="2dy6Wl71BPd" role="1_xcdC">
      <property role="1$DGzf" value="45" />
      <property role="1$DGzd" value="304" />
      <property role="TrG5h" value="MyComponent" />
      <node concept="1$DGXZ" id="2dy6Wl71BPe" role="1$DGXN" />
      <node concept="1$DGXW" id="2dy6Wl71BPf" role="1$DGXT" />
    </node>
    <node concept="1$DGuN" id="2dy6Wl71BPs" role="1_xcdE">
      <ref role="1$DGuM" node="2dy6Wl6VYyi" resolve="Some Component" />
      <ref role="1$DGuK" node="2dy6Wl71BPd" resolve="MyComponent" />
    </node>
    <node concept="1$DGuN" id="2dy6Wl71BPF" role="1_xcdE">
      <ref role="1$DGuM" node="2dy6Wl71BPd" resolve="MyComponent" />
      <ref role="1$DGuK" node="2dy6Wl6Xbaq" />
    </node>
    <node concept="1$DGtV" id="2dy6Wl728FL" role="1_xcdC">
      <property role="1$DGzf" value="206" />
      <property role="1$DGzd" value="180" />
      <node concept="1$DGXZ" id="2dy6Wl728FM" role="1$DGXN" />
      <node concept="1$DGXW" id="2dy6Wl728FN" role="1$DGXT" />
      <node concept="1$DGXZ" id="2dy6Wl72v8C" role="1$DGXN" />
      <node concept="1$DGXW" id="2dy6Wl73Kyv" role="1$DGXT" />
      <node concept="1$DGXW" id="2dy6Wl73KyT" role="1$DGXT" />
      <node concept="1$DGXW" id="2dy6Wl74zcO" role="1$DGXT" />
      <node concept="1$DGXW" id="2dy6Wl74zdh" role="1$DGXT" />
    </node>
    <node concept="1$DGtV" id="2dy6Wl72v7U" role="1_xcdC">
      <property role="1$DGzf" value="38" />
      <property role="1$DGzd" value="213" />
      <node concept="1$DGXZ" id="2dy6Wl72v7V" role="1$DGXN" />
      <node concept="1$DGXW" id="2dy6Wl72v7W" role="1$DGXT" />
      <node concept="1$DGXW" id="2dy6Wl72v8g" role="1$DGXT" />
      <node concept="1$DGXW" id="2dy6Wl72v8B" role="1$DGXT" />
    </node>
    <node concept="1$DGuN" id="2dy6Wl72v8f" role="1_xcdE">
      <ref role="1$DGuM" node="2dy6Wl72v7U" />
      <ref role="1$DGuK" node="2dy6Wl6Xbaq" />
    </node>
    <node concept="1$DGuN" id="2dy6Wl72v8A" role="1_xcdE">
      <ref role="1$DGuM" node="2dy6Wl72v7U" />
      <ref role="1$DGuK" node="2dy6Wl728FL" />
    </node>
    <node concept="1$DGuN" id="2dy6Wl73Kyu" role="1_xcdE">
      <ref role="1$DGuM" node="2dy6Wl728FL" />
      <ref role="1$DGuK" node="2dy6Wl6Xbaq" />
    </node>
    <node concept="1$DGuN" id="2dy6Wl73KyS" role="1_xcdE">
      <ref role="1$DGuM" node="2dy6Wl728FL" />
      <ref role="1$DGuK" node="2dy6Wl6VYyc" />
    </node>
    <node concept="1$DGtV" id="2dy6Wl74bZk" role="1_xcdC">
      <property role="1$DGzf" value="817" />
      <property role="1$DGzd" value="59" />
      <node concept="1$DGXZ" id="2dy6Wl74bZl" role="1$DGXN" />
      <node concept="1$DGXW" id="2dy6Wl74bZm" role="1$DGXT" />
      <node concept="1$DGXZ" id="2dy6Wl74zcP" role="1$DGXN" />
      <node concept="1$DGXZ" id="2dy6Wl74zdi" role="1$DGXN" />
      <node concept="1$DGXZ" id="2dy6Wl74zdM" role="1$DGXN" />
      <node concept="1$DGXZ" id="3DjWPY73YjU" role="1$DGXN" />
    </node>
    <node concept="1$DGuN" id="2dy6Wl74zcN" role="1_xcdE">
      <property role="1$q0JZ" value="0" />
      <property role="1$q0Jx" value="2" />
      <ref role="1$DGuM" node="2dy6Wl728FL" />
      <ref role="1$DGuK" node="2dy6Wl74bZk" />
    </node>
    <node concept="1$DGuN" id="2dy6Wl74zdg" role="1_xcdE">
      <property role="1$q0JZ" value="0" />
      <property role="1$q0Jx" value="3" />
      <ref role="1$DGuM" node="2dy6Wl728FL" />
      <ref role="1$DGuK" node="2dy6Wl74bZk" />
    </node>
    <node concept="1$DGuN" id="2dy6Wl74zdK" role="1_xcdE">
      <property role="1$q0JZ" value="1" />
      <property role="1$q0Jx" value="0" />
      <ref role="1$DGuM" node="2dy6Wl6VYyc" />
      <ref role="1$DGuK" node="2dy6Wl74bZk" />
    </node>
    <node concept="1$DGuN" id="2dy6Wl74zef" role="1_xcdE">
      <property role="1$q0JZ" value="2" />
      <property role="1$q0Jx" value="1" />
      <ref role="1$DGuM" node="2dy6Wl6VYyc" />
      <ref role="1$DGuK" node="2dy6Wl6Xbaq" />
    </node>
    <node concept="1$DGtV" id="5FGZySRolPH" role="1_xcdC">
      <property role="1$DGzf" value="21" />
      <property role="1$DGzd" value="31" />
      <node concept="1$DGXZ" id="5FGZySRolPI" role="1$DGXN" />
      <node concept="1$DGXW" id="5FGZySRolPJ" role="1$DGXT" />
    </node>
    <node concept="1$DGtV" id="5FGZySRolTm" role="1_xcdC">
      <property role="1$DGzf" value="176" />
      <property role="1$DGzd" value="-28" />
      <node concept="1$DGXZ" id="5FGZySRolTn" role="1$DGXN" />
      <node concept="1$DGXW" id="5FGZySRolTo" role="1$DGXT" />
    </node>
    <node concept="1$DGtV" id="5FGZySRolU5" role="1_xcdC">
      <property role="1$DGzf" value="1" />
      <property role="1$DGzd" value="122" />
      <node concept="1$DGXZ" id="5FGZySRolU6" role="1$DGXN" />
      <node concept="1$DGXW" id="5FGZySRolU7" role="1$DGXT" />
    </node>
    <node concept="1$DGtV" id="4Q9edAs9QPp" role="1_xcdC">
      <property role="1$DGzf" value="439" />
      <property role="1$DGzd" value="242" />
      <node concept="1$DGXZ" id="4Q9edAs9QPq" role="1$DGXN" />
      <node concept="1$DGXW" id="4Q9edAs9QPr" role="1$DGXT" />
      <node concept="1$DGXW" id="4Q9edAs9TuS" role="1$DGXT" />
    </node>
    <node concept="1$DGtV" id="4Q9edAs9Tt6" role="1_xcdC">
      <property role="1$DGzf" value="583" />
      <property role="1$DGzd" value="199" />
      <node concept="1$DGXZ" id="4Q9edAs9Tt7" role="1$DGXN" />
      <node concept="1$DGXW" id="4Q9edAs9Tt8" role="1$DGXT" />
      <node concept="1$DGXZ" id="4Q9edAs9TuT" role="1$DGXN" />
      <node concept="1$DGXW" id="3DjWPY73YjT" role="1$DGXT" />
    </node>
    <node concept="1$DGtV" id="4Q9edAs9TtX" role="1_xcdC">
      <property role="1$DGzf" value="595" />
      <property role="1$DGzd" value="290" />
      <node concept="1$DGXZ" id="4Q9edAs9TtY" role="1$DGXN" />
      <node concept="1$DGXW" id="4Q9edAs9TtZ" role="1$DGXT" />
    </node>
    <node concept="1$DGuN" id="4Q9edAs9TuR" role="1_xcdE">
      <property role="1$q0JZ" value="0" />
      <property role="1$q0Jx" value="0" />
      <ref role="1$DGuM" node="4Q9edAs9QPp" />
      <ref role="1$DGuK" node="4Q9edAs9Tt6" />
    </node>
    <node concept="1$DGtV" id="4Q9edAs9U3B" role="1_xcdC">
      <property role="1$DGzf" value="372" />
      <property role="1$DGzd" value="333" />
      <node concept="1$DGXZ" id="4Q9edAs9U3C" role="1$DGXN" />
      <node concept="1$DGXW" id="4Q9edAs9U3D" role="1$DGXT" />
      <node concept="1$DGXZ" id="4Q9edAsbtl_" role="1$DGXN" />
    </node>
    <node concept="1$DGtV" id="4Q9edAs9UFw" role="1_xcdC">
      <property role="1$DGzf" value="234" />
      <property role="1$DGzd" value="372" />
      <node concept="1$DGXZ" id="4Q9edAs9UFx" role="1$DGXN" />
      <node concept="1$DGXW" id="4Q9edAs9UFy" role="1$DGXT" />
      <node concept="1$DGXW" id="4Q9edAsbtl$" role="1$DGXT" />
    </node>
    <node concept="1$DGuN" id="4Q9edAsbtlz" role="1_xcdE">
      <property role="1$q0JZ" value="0" />
      <property role="1$q0Jx" value="0" />
      <ref role="1$DGuM" node="4Q9edAs9UFw" />
      <ref role="1$DGuK" node="4Q9edAs9U3B" />
    </node>
    <node concept="1$DGuN" id="3DjWPY73YjS" role="1_xcdE">
      <property role="1$q0JZ" value="2" />
      <property role="1$q0Jx" value="0" />
      <ref role="1$DGuM" node="4Q9edAs9Tt6" />
      <ref role="1$DGuK" node="2dy6Wl74bZk" />
    </node>
  </node>
  <node concept="36pFFh" id="4Q9edAs9jAH">
    <node concept="3hEjIK" id="4Q9edAs9jAI" role="36pFFi">
      <property role="3hEjH4" value="494" />
      <property role="3hEjHS" value="-75" />
      <property role="TrG5h" value="NEW" />
    </node>
    <node concept="3hEjIK" id="4Q9edAs9jAK" role="36pFFi">
      <property role="3hEjH4" value="303" />
      <property role="3hEjHS" value="-99" />
      <property role="TrG5h" value="Box1" />
    </node>
    <node concept="3hEjIK" id="4Q9edAsbtm3" role="36pFFi">
      <property role="3hEjH4" value="426" />
      <property role="3hEjHS" value="302" />
      <property role="TrG5h" value="NEW" />
    </node>
    <node concept="3hEjIK" id="4Q9edAsdoGI" role="36pFFi">
      <property role="3hEjH4" value="76" />
      <property role="3hEjHS" value="116" />
      <property role="TrG5h" value="NEW" />
      <node concept="3hIHTD" id="4Q9edAsdoGJ" role="3hIHTR" />
      <node concept="3hIHTC" id="4Q9edAsdoGK" role="3hIHTF" />
    </node>
    <node concept="3hEjIK" id="4Q9edAsdoGW" role="36pFFi">
      <property role="3hEjH4" value="393" />
      <property role="3hEjHS" value="9" />
      <property role="TrG5h" value="NEW" />
      <node concept="3hIHTD" id="4Q9edAsdoGX" role="3hIHTR" />
      <node concept="3hIHTC" id="4Q9edAsdoGY" role="3hIHTF" />
    </node>
    <node concept="3hFbwb" id="4Q9edAsdpmD" role="36pFFG">
      <property role="3hCCMY" value="OutputPort" />
      <property role="3hCCMK" value="InputPort" />
      <ref role="36pFFv" node="4Q9edAsdoGI" resolve="NEW" />
      <ref role="36pFFp" node="4Q9edAsdoGW" resolve="NEW" />
    </node>
    <node concept="3hEjIK" id="4Q9edAsdzGN" role="36pFFi">
      <property role="3hEjH4" value="83" />
      <property role="3hEjHS" value="21" />
      <property role="TrG5h" value="NEW" />
      <node concept="3hIHTD" id="4Q9edAsdzGO" role="3hIHTR" />
      <node concept="3hIHTC" id="4Q9edAsdzGP" role="3hIHTF" />
    </node>
    <node concept="3hFbwb" id="4Q9edAsdzH2" role="36pFFG">
      <property role="3hCCMY" value="OutputPort" />
      <property role="3hCCMK" value="InputPort" />
      <ref role="36pFFv" node="4Q9edAsdzGN" resolve="NEW" />
      <ref role="36pFFp" node="4Q9edAsdoGW" resolve="NEW" />
    </node>
    <node concept="3hEjIK" id="4Q9edAsdHqW" role="36pFFi">
      <property role="3hEjH4" value="508" />
      <property role="3hEjHS" value="150" />
      <property role="TrG5h" value="NEW" />
      <node concept="3hIHTD" id="4Q9edAsdHqX" role="3hIHTR" />
      <node concept="3hIHTC" id="4Q9edAsdHqY" role="3hIHTF" />
    </node>
    <node concept="3hFbwb" id="4Q9edAsdHre" role="36pFFG">
      <property role="3hCCMY" value="OutputPort" />
      <property role="3hCCMK" value="InputPort" />
      <ref role="36pFFv" node="4Q9edAsdoGW" resolve="NEW" />
      <ref role="36pFFp" node="4Q9edAsdHqW" resolve="NEW" />
    </node>
    <node concept="3hFbwb" id="3DjWPY73Ukc" role="36pFFG">
      <ref role="36pFFv" node="4Q9edAsbtm3" resolve="NEW" />
      <ref role="36pFFp" node="4Q9edAsdHqW" resolve="NEW" />
    </node>
  </node>
</model>

